<?php
     include 'function/function_db.php';
    $function_db = new function_db();  
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];

      // echo "GET ID READ:".$_GET['id'];
    }
     
    if ( null==$id ) {
        header("Location: create.php");
    } else {
        $data = $function_db->getuserlist($id);
    }
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link   href="../../css/bootstrap.min.css" rel="stylesheet">
    <script src="../../js/bootstrap.min.js"></script>
     <title>Info</title>
  <link rel="icon" type="image/png" href="../../img/teras.png"/>
</head>
 
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>Information</h3>
                    </div>
                     
                    <div class="form-horizontal" >
                      <div class="control-group">
                        <label class="control-label">Name</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['users_names'];?>
                            </label>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">EPC-TAGID</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['users_tagid'];?>
                            </label>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">MFG No</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['employee_id'];?>
                            </label>
                        </div>
                      </div>
                          <div class="control-group">
                        <label class="control-label">Plate No</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['users_plno'];?>
                            </label>
                        </div>
                      </div>

                         <div class="control-group">
                        <label class="control-label">User Status</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['users_status'];?>
                            </label>
                        </div>
                      </div>

                   <div class="control-group">
                        <label class="control-label">Org</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['users_org'];?>
                            </label>
                        </div>
                      </div>

                        <div class="form-actions">
                          <a class="btn" href="index.php">Back</a>
                        <!--   <a class="btn btn-success" href="update.php?id=<?php echo $_GET['id']; ?> ">Update</a> -->
                       </div>
                     
                      
                    </div>
                </div>
                 
    </div> <!-- /container -->
  </body>
</html>