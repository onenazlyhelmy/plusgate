/*------------------------------------------------------
    Author : www.webthemez.com
    License: Commons Attribution 3.0
    http://creativecommons.org/licenses/by/3.0/
---------------------------------------------------------  */

(function ($) {
    "use strict";
    var mainApp = {

        initFunction: function () {
            /*MENU 
            ------------------------------------*/
            $('#main-menu').metisMenu();
			
            $(window).bind("load resize", function () {
                if ($(this).width() < 768) {
                    $('div.sidebar-collapse').addClass('collapse')
                } else {
                    $('div.sidebar-collapse').removeClass('collapse')
                }
            });

          
          
            Morris.Line({
                element: 'drac-line-chart',
                data: [
					  { y: '2016-04-01', a: 50, b: 90,c: 20},
					  { y: '2016-04-02', a: 165,  b: 185,c: 30},
					  { y: '2016-04-03', a: 150,  b: 130,c: 50},
					  { y: '2016-04-04', a: 175,  b: 160,c: 10},
					  { y: '2016-04-05', a: 80,  b: 65,c: 70},
					  { y: '2016-04-06', a: 90,  b: 70,c: 190},
					  { y: '2016-04-07', a: 100, b: 125,c: 90},
					  { y: '2016-04-08', a: 155, b: 175,c: 70},
					  { y: '2016-04-09', a: 80, b: 85,c: 90},
					  { y: '2016-04-10', a: 145, b: 155,c: 100},
					  { y: '2016-04-11', a: 160, b: 195,c: 40}
				],
            
				 
      xkey: 'y',
      ykeys: ['a', 'b','c'],
      labels: ['Annex 1', 'Annex 2', 'Annex 3'],
      fillOpacity: 0.6,
      hideHover: 'auto',
      behaveLikeLine: true,
      resize: true,
      pointFillColors:['#ffffff'],
      pointStrokeColors: ['black'],
      lineColors:['gray','#FB6262','#5B0062']
	  
            });
           
     
        },

        initialization: function () {
            mainApp.initFunction();

        }

    }
    // Initializing ///

    $(document).ready(function () {
        mainApp.initFunction(); 
		$("#sideNav").click(function(){
			if($(this).hasClass('closed')){
				$('.navbar-side').animate({left: '0px'});
				$(this).removeClass('closed');
				$('#page-wrapper').animate({'margin-left' : '260px'});
				
			}
			else{
			    $(this).addClass('closed');
				$('.navbar-side').animate({left: '-260px'});
				$('#page-wrapper').animate({'margin-left' : '0px'}); 
			}
		});
    });

}(jQuery));
