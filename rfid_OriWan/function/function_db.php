<?php
include 'database.php';

class function_db extends PDO {

	public static $pdo = null ;
	public static $IParr=array('10.10.10.141','10.10.10.42','10.10.10.43','10.10.10.44','10.10.10.145');
	public static $rabb_qname=array('useranx1','useranx2','useranx3','userpost1','userpost2');
		// public static $IParr=array('10.224.56.64','10.224.56.65','10.224.56.69','10.224.56.70','10.224.56.71');
	public static $dbpath="/home/pi/rfidGA/GA/jGA.jar.db";

	 #init..dbconn
	 public function __construct() {
        self::$pdo = Database::connect();
    }


        public static function getcurr_db() {


		    return Database::getdb();


		    }
       public static function get_piIPlist() {


		    return   self::$IParr;


		    }
		     public static function get_pi123() {


		    return   self::$rabb_qname;


		    }
 public static function get_dbpath() {


		    return   self::$dbpath;


		    }

    public static function reg_user($users_name,$users_tagid,$employee_id,$users_department,$tarikh_reg,$users_plno,$users_org,$users_remark,$users_acc_loc,$users_acc_lvl) {


			try {


	        $pdo = self::$pdo;
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO users (users_names,users_tagid,employee_id,users_department,tarikh_reg,users_status,users_plno,users_org,users_remark,users_acc_loc,users_acc_lvl)
			values(?,?,?,?,?,?,?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($users_name,$users_tagid,$employee_id,$users_department,$tarikh_reg,'OK',$users_plno,$users_org,$users_remark,$users_acc_loc,$users_acc_lvl));
			//return "Success update..!!";
             $lastid = $pdo->lastInsertId();
             return $lastid;
			Database::disconnect();
			}
	        	catch(PDOException $e)
			{
				return $e->getMessage();
			}





		    }

		       public static function reg_user_M($users_M_name,$users_M_pwd,$users_M_priv,$users_M_remark,$users_M_desc,$users_M_stat,$users_M_dt) {


			try {


	        $pdo = self::$pdo;
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO users_M(users_M_name,users_M_pwd,users_M_priv,users_M_remark,users_M_desc,users_M_stat,users_M_dt)
			values('$users_M_name','$users_M_pwd','$users_M_priv','$users_M_remark','$users_M_desc','$users_M_stat','$users_M_dt')";
			$q = $pdo->prepare($sql);
			$q->execute();
			// echo "SQL:..!! ".$sql."<br>";
            $lastid = $pdo->lastInsertId();
             return $lastid;
			 //return "OK";
			Database::disconnect();
			}
	        	catch(PDOException $e)
			{
				return $e->getMessage();
			}





		    }

		public static function updateusers($idusertag,$users_names,$users_tagid,$employee_id,$users_plno,$users_department,$users_status,$users_org,$users_remark,$users_acc_loc,$users_acc_lvl) {


			try {

	        $pdo = self::$pdo;
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE users  set users_names = '$users_names', users_tagid = '$users_tagid', employee_id ='$employee_id', users_department ='$users_department',users_plno='$users_plno' ,
			users_status='$users_status', users_org='$users_org' ,users_remark='$users_remark' ,users_acc_loc='$users_acc_loc',users_acc_lvl='$users_acc_lvl'  WHERE idusertag = '$idusertag';";
			$q = $pdo->prepare($sql);
			$q->execute();
			return "Success update..!!";
            //echo "Success update..!!";
			Database::disconnect();
			}
	        	catch(PDOException $e)
			{
				return $e->getMessage();
			}





		    }

		public static function updateusers_M($users_M_name,$users_M_pwd,$users_M_priv,$users_M_remark,$users_M_desc,$users_M_stat,$users_M_dt,$idusers_M) {


			try {

	        $pdo = self::$pdo;
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE users_M  set users_M_name = '$users_M_name', users_M_pwd = '$users_M_pwd', users_M_priv ='$users_M_priv', users_M_remark ='$users_M_remark',users_M_stat='$users_M_stat' ,
			users_M_dt='$users_M_dt' WHERE idusers_M = $idusers_M;";
			$q = $pdo->prepare($sql);
			$q->execute();
			//return "Success update..!!";
            echo "update?..!!" .$sql;
            //die();
			Database::disconnect();
			}
	        	catch(PDOException $e)
			{
				return $e->getMessage();
			}





		    }
		       public static function graphmon_insert($dt,$k01,$k02,$k03,$k04,$k05,$m01,$m02,$m03,$m04,$m05) {


			try {

	        $pdo = self::$pdo;
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO graphmon (dt,K01,K02,K03,K04,K05,M01,M02,M03,M04,M05)
			values(?,?,?,?,?,?,?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($dt,$k01,$k02,$k03,$k04,$k05,$m01,$m02,$m03,$m04,$m05));
			//return "Success update..!!";

			Database::disconnect();
			}
	        	catch(PDOException $e)
			{
				return $e->getMessage();
			}


		    }

	  	public static function graphmon_del($id) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")

			try {

	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "DELETE FROM graphmon";
            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            #$data = $q->fetchall();

			return "Delete success !";
			#Database_local::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }
		public static function graphmon_chcklatest($dt) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")

			try {

	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          //  $sql = "SELECT count(dt) FROM graphmon ";
            $sql = "SELECT count(dt) FROM graphmon WHERE dt='$dt'";
            $q = $pdo->prepare($sql);
            $q->execute();
            $data = $q->fetch(PDO::FETCH_ASSOC);
            #$data = $q->fetchall();

			return $data;
			#Database_local::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }




    	public static function getuserlist($id) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")

			try {

	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT * FROM users WHERE idusertag='$id'";
            $q = $pdo->prepare($sql);
            $q->execute();
            $data = $q->fetch(PDO::FETCH_ASSOC);
            #$data = $q->fetchall();

			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }

		     	public static function getloglist($id) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")

			try {

	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT * FROM raw_log WHERE idraw_log='$id'";
            $q = $pdo->prepare($sql);
            $q->execute();
            $data = $q->fetch(PDO::FETCH_ASSOC);
            #$data = $q->fetchall();

			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }

		       	public static function getuserlist_M($id) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")

			try {

	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT * FROM users_M WHERE idusers_M='$id'";
            $q = $pdo->prepare($sql);
            $q->execute();
            $data = $q->fetch(PDO::FETCH_ASSOC);
            #$data = $q->fetchall();

			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }


    	public static function deleteusers($id) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")

			try {

	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "DELETE FROM users WHERE idusertag='$id'";
            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            #$data = $q->fetchall();

			return "success !";
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }

		 	public static function deleteusers_M($id) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")

			try {

	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "DELETE FROM users_M WHERE idusers_M='$id'";
            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            #$data = $q->fetchall();

			return "success !";
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }

		  public static function reportcsv($sql_str) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")

			try {

	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //     $sql = "SELECT * from stardb.raw_log WHERE
       //  STR_TO_DATE(raw_tarikh,'%d/%m/%Y %H:%i:%s') between STR_TO_DATE('$datefrom 00:00:00', '%d/%m/%Y %H:%i:%s')
        //            AND STR_TO_DATE('$dateto 23:59:00', '%d/%m/%Y %H:%i:%s') $sql_str;";

            $q = $pdo->prepare($sql_str);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            $data = $q->fetchall();

			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }
           public static function reportnamecsv($users_names,$sql_path) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")

			try {

	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT * FROM stardb.raw_log WHERE raw_name like '".$users_names."' ".$sql_path.
              " GROUP BY ROUND(UNIX_TIMESTAMP(STR_TO_DATE(raw_tarikh,'%d/%m/%Y %H:%i:%s'))/40) ORDER BY idraw_log DESC";

            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            $data = $q->fetchall();

			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }
		    public static function dummyreportcsv($datefrom,$dateto) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")

			try {

	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT * from stardb.raw_dummy WHERE aw_name <>'unknown' AND
             STR_TO_DATE(tarikh,'%d/%m/%Y %H:%i:%s') between STR_TO_DATE('$datefrom 00:00:00', '%d/%m/%Y %H:%i:%s')
             AND STR_TO_DATE('$dateto 23:59:00', '%d/%m/%Y %H:%i:%s') ORDER BY tarikh DESC;";

            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            $data = $q->fetchall();

			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }

		   public static function chck_employee_id($employee_id) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")

			try {



	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT count(employee_id) from users WHERE employee_id='".$employee_id."'";

            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            $data = $q->fetchall();

			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }
   public static function chck_employee_id_update($employee_id,$idusertag) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")

			try {



	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     $sql = "SELECT count(employee_id) from users WHERE employee_id='".$employee_id."' AND idusertag <> ".$idusertag;

            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            $data = $q->fetchall();

			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }

		     public static function GCMsend($message,$regID) {

               # $message = $_POST['message'];

		        	$apiKey = "AIzaSyA1UlYyaS1h07iyeRvpexwR4p8x0Laik0w";

					// Set POST variables
					$url = 'https://android.googleapis.com/gcm/send';

					$fields = array(
					                'registration_ids'  => array($regID),
					                'data'              => array( "message" => $message ),
					                );

					$headers = array(
					                    'Authorization: key=' . $apiKey,
					                    'Content-Type: application/json'
					                );

					// Open connection
					$ch = curl_init();

					// Set the url, number of POST vars, POST data
					curl_setopt( $ch, CURLOPT_URL, $url );

					curl_setopt( $ch, CURLOPT_POST, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

					curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );

					// Execute post
					$result = curl_exec($ch);

					        if ($result === FALSE) {
					            die('Curl failed: ' . curl_error($ch));
					        }

					// Close connection
					curl_close($ch);

					echo $result;

		     }


}

?>
