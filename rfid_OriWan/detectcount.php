
    <?php 
include 'ss/ss.php';
   
require_once 'function/getdatagraph.php';
$con = mysqli_connect('10.224.150.64', 'roots', 'root123','stardb');
$rows = '';
$query = "SELECT * FROM graphmon ORDER BY graphmonid DESC LIMIT 0, 9";
$result = mysqli_query($con,$query);
$total_rows =  $result->num_rows;
if($result) 
{
    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
}
#print_r($rows);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <title>Teras Log</title>
	<link rel="icon" type="image/png" href="img/teras.png"/>
   <link href="css/pagination.css" rel="stylesheet">
  <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-1.10.2.min.js"></script>

 <script type="text/javascript" src="js/smoothie.js"></script>
    

  <link rel="icon" type="image/png" href="img/teras.png"/>
    <!-- Bootstrap Styles-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="assets/css/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- Google Fonts-->
    
    <link href="css/fixtab.css" rel="stylesheet">

     
    <!-- Metis Menu Js -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>  

     <!-- Custom Js -->
    <script src="assets/js/left-pane-slide.js"></script>

</head>



<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><i class="fa fa-home"></i> <strong>GRAPH</strong></a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                
        </nav>

        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
		<div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                  <li align='left'>
                        <a href="index.php"><i class="fa fa-dashboard"></i> Dashboard   </a>
                    </li>
          
          <li align='left'>          
                        <a href="detectionCnt.php"><i class="fa fa-dashboard"></i> Detection Graph</a> 
                    </li>
                        
                    <li align='left'>
                        <a href="log.php"><i class="fa fa-dashboard"></i> Transactions Log </a>
                    </li>
                    <li align='left'>
                        <a href="user/create.php"  ><i class="fa fa-users"></i> Registration Users</a>
                    </li>

                     <li align='left'>
                        <a href="user/user.php" <?php echo $style; ?> ><i class="fa fa-users"></i> User Mgmnt</a>
                    </li>
                         <li align='left'>
                        <a href="logout666.php"><i class="fa fa-users"></i> Logout</a>
                    </li>
                   <!--  <li>                    
                        <a href="hdwareinfo/realtimehdinfo.php"><i class="fa fa-home"></i>Hardware Info</a> 
                    </li>									
					<li>
                        <a href="#"><i class="fa fa-user"></i> Administration<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="create.php">User Maintenance</a>
                            </li>
                            <li>
                                <a href="wbrmt.html">WEB Remote</a>
                            </li>
                           </ul>

                      </li> -->
             
                        </ul>
                    </li>
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <!--<h1 class="page-header">
                            Dashboard <u><small>Summary of RFID LOG</small></u>
                                <br><img src='img/parrotsec.png' height='30px' width='30px'>
  <a href='https://www.parrotsec.org/' style="color:blue;font-size: 8pt; font-family:Lucida Console,Monaco, monospace;" > Powered by parrot security</a>
                        </h1>
						<ol class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li><a href="#">Library</a></li>
						<li class="active">Data</li>
						</ol>-->
                    </div>
                </div>
				
				
                <!-- /. ROW  -->
				<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							RFID Detection counts
						</div>
						<div class="panel-body">
                        
							<div class="loading" id="drac-line-chart"></div>
						</div>
					</div>  
					</div>		
				</div> 
				
               
                <!-- /. ROW  -->
                <!-- /. ROW  -->
				<!--<footer><p>TERAS RFID LOG</p></footer>-->
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->

	
 
   <!--   <script src="assets/js/son_of_bitch.js"></script>  -->

    <script>

         Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'drac-line-chart',
     
        // Chart data records -- each entry in this array corresponds to a point
        // on the chart.
        data: <?php echo json_encode($rows);?>,
     
        // The name of the data record attribute that contains x-values.
        xkey: 'dt',
     
        // A list of names of data record attributes that contain y-values.
        ykeys: ['K01','K02','K03','K04','K05','M01','M02','M03','M04','M05'],
     
        // Labels for the ykeys -- will be displayed when you hover over the
        // chart.
        labels: ['Annex 1 Exit:','Annex 2 Exit','Annex 3 Exit','Gate 1 Access Exit ','Gate 2 Access Exit','Annex 1 Entry:','Annex 2 Entry','Annex 3 Entry','Gate 1 Access Entry ','Gate 2 Access Entry'],
     
        lineColors: ['#0b62a4'],
        xLabels: 'dt',
         fillOpacity: 0.9,
         hideHover: 'auto',
         behaveLikeLine: true,
         resize: true,
         pointFillColors:['#ffffff'],
         pointStrokeColors: ['black'],
         lineColors:['#FB6362','#FB6262','#5B0062','#FB6342','#FB6360','#DD6362','#FB8342','#FB6399','#FB6362','#AB6362'],
     
        // Disables line smoothing
        smooth: true,
        resize: true
           });
  
  

</script>
</body>

</html>
