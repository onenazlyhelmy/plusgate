 <link rel="icon" type="image/png" href="../img/teras.png"/>
  <title>Hardware info</title>
  <link href="../css/fixtab.csssss" rel="stylesheet">


<style>
  .btn {
  display: inline-block;
  *display: inline;
  padding: 4px 12px;
  margin-bottom: 0;
  *margin-left: .3em;
  font-size: 14px;
  line-height: 20px;
  color: #333444;
  text-align: center;
  text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
  vertical-align: middle;
  cursor: pointer;
  background-color: #f5f5f5;
  *background-color: #e6e6e6;
  background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6));
  background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);
  background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);
  background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);
  background-repeat: repeat-x;
  border: 1px solid #cccccc;
  *border: 0;
  border-color: #e6e6e6 #e6e6e6 #bfbfbf;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  border-bottom-color: #b3b3b3;
  -webkit-border-radius: 4px;
     -moz-border-radius: 4px;
          border-radius: 4px;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe6e6e6', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
  *zoom: 1;
  -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
     -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
          box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
      }
  .btn-primary.active,
.btn-warning.active,
.btn-danger.active,
.btn-success.active,
.btn-info.active,
.btn-inverse.active {
  color: rgba(255, 255, 255, 0.75);
}

.btn-danger {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #da4f49;
  *background-color: #bd362f;
  background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#bd362f));
  background-image: -webkit-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);
  background-repeat: repeat-x;
  border-color: #bd362f #bd362f #802420;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffbd362f', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.btn-success {
  color: #fff333;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #5bb75b;
  *background-color: #51a351;
  background-image: -moz-linear-gradient(top, #62c462, #51a351);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#62c462), to(#51a351));
  background-image: -webkit-linear-gradient(top, #62c462, #51a351);
  background-image: -o-linear-gradient(top, #62c462, #51a351);
  background-image: linear-gradient(to bottom, #62c462, #51a351);
  background-repeat: repeat-x;
  border-color: #51a351 #51a351 #387038;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462', endColorstr='#ff51a351', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.btn-success:hover,
.btn-success:focus,
.btn-success:active,
.btn-success.active,
.btn-success.disabled,
.btn-success[disabled] {
  color: #ffffff;
  background-color: #51a351;
  *background-color: #499249;
}


.btn-success:active,
.btn-success.active {
  background-color: #408140 \9;
}
button.btn,
input[type="submit"].btn {
  *padding-top: 3px;
  *padding-bottom: 3px;
}
</style>


 <script src="../js/jquery-1.10.2.min.js"></script>
 <script type="text/javascript" src="../js/smoothie.js"></script>
 <br>
 <a href="../index.php" button class="btn btn-danger">Back</a>
  <a href="../log.php" button class="btn btn-success">Report/Log</a>
  <a button class="btn btn-success" href="../user/user.php">User Mgmnt</a>

<a button class="btn btn-success" href="../realtime.php">Realtime </a>
<a button class="btn btn-danger" href="../ssh/proc.php?strip=000000">Pi Current process </a>
<a button class="btn btn-danger" href="../ssh/updatesoft.php?strip=000000">Update App </a>


<script type="text/javascript">
var auto_refresh = setInterval(
function ()
{
$('#redis10_10_10_42').load('redis10_10_10_42.php').fadeIn("slow");
}, 500);

var auto_refresh = setInterval(
function ()
{
$('#redis10_10_10_41').load('redis10_10_10_41.php').fadeIn("slow");
}, 500);

var auto_refresh = setInterval(
function ()
{
$('#redis10_10_10_44').load('redis10_10_10_44.php').fadeIn("slow");
}, 500);

var auto_refresh = setInterval(
function ()
{
$('#redis10_10_10_45').load('redis10_10_10_45.php').fadeIn("slow");
}, 500);

  var auto_refresh = setInterval(
      function ()
      {

     $('#redis10_10_10_43').load('redis10_10_10_43.php').fadeIn("slow");
     }, 500);

      // Randomly add a data point every 500ms------------------------------------



    </script>

<?php
   $date = new DateTime('now',new DateTimeZone('Singapore'));
   $dt= $date->format('Ymd');
   $dtlog = $dt."_ga.txt";


?>
<table border=  class = "alert alert-info"><tr><td>

</table>
</body>

<div class="">
                        <!-- block -->
                        <div class="">
                            <div class="">

                            </div>
                            <div class="">

  									<table  >
						              <thead style="width:100%;background-color:#DAC4C7; " >
						                <tr>
						                  <th> <img height='70' width='90' src='../img/teras.png' style="font-size:14px; font-weight: bold;line-height: 142%;padding: 1px"></th>
						                  <th  style="font-size:14px; font-weight: bold;line-height: 142%;padding: 1px">ANNEX 1
                              <br><a button class="btn btn-success" href="../ssh/killremotethrussh.php?ip=10.10.10.141">Reset Remote</a>
                              <br><a button class="btn btn-success" href="../ssh/logthrussh.php?ip=10.10.10.141&dt=ga">Log</a>
                              <br><a button class="btn btn-success" href="../ssh/updatesoft.php?strip=10.10.10.141&dt=ga">UpdateSoftware</a>
<br><a button class="btn btn-danger" href="../ssh/softkill.php?strip=10.10.10.141&dt=ga">Restart Software</a>
<br><a button class="btn btn-danger" href="../ssh/settingcheck.php?strip=10.10.10.141&dt=ga">Check Setting</a>
                               </th>

						                  <th style="font-size:14px; font-weight: bold;line-height: 142%;padding: 1px">ANNEX 2
                              <br><a button class="btn btn-success" href="../ssh/killremotethrussh.php?ip=10.10.10.42">Reset Remote</a>
                               <br><a button class="btn btn-success" href="../ssh/logthrussh.php?ip=10.10.10.42&dt=ga" >Log</a>
 <br><a button class="btn btn-success" href="../ssh/updatesoft.php?strip=10.10.10.42&dt=ga">UpdateSoftware</a>
<br><a button class="btn btn-danger" href="../ssh/softkill.php?strip=10.10.10.42&dt=ga">Restart Software</a>
<br><a button class="btn btn-danger" href="../ssh/settingcheck.php?strip=10.10.10.42&dt=ga">Check Setting</a>
                              </th>

						                  <th style="font-size:14px; font-weight: bold;line-height: 142%;padding: 1px">FRONT GATE
                              <br><a button class="btn btn-success" href="../ssh/killremotethrussh.php?ip=10.10.10.44">Reset Remote</a>
                                   <br><a button class="btn btn-success" href="../ssh/logthrussh.php?ip=10.10.10.44&dt=ga">Log</a>
 <br><a button class="btn btn-success" href="../ssh/updatesoft.php?strip=10.10.10.44&dt=ga">UpdateSoftware</a>
<br><a button class="btn btn-danger" href="../ssh/softkill.php?strip=10.10.10.44&dt=ga">Restart Software</a>
<br><a button class="btn btn-danger" href="../ssh/settingcheck.php?strip=10.10.10.44&dt=ga">Check Setting</a>
                              </th>
						                  <th style="font-size:14px; font-weight: bold;line-height: 142%;padding: 1px">BACKGATE
                               <br><a button class="btn btn-success" href="../ssh/killremotethrussh.php?ip=10.10.10.145">Reset Remote</a>
                                <br><a button class="btn btn-success" href="../ssh/logthrussh.php?ip=10.10.10.145&dt=ga">Log</a>
 <br><a button class="btn btn-success" href="../ssh/updatesoft.php?strip=10.10.10.145&dt=ga">UpdateSoftware</a>
<br><a button class="btn btn-danger" href="../ssh/softkill.php?strip=10.10.10.145&dt=ga">Restart Software</a>
<br><a button class="btn btn-danger" href="../ssh/settingcheck.php?strip=10.10.10.145&dt=ga">Check Setting</a>
                               </th>
										  <th style="font-size:14px; font-weight: bold;line-height: 142%;padding: 1px">ANNEX 3
      <br><a button class="btn btn-success" href="../ssh/killremotethrussh.php?ip=10.10.10.43">Reset Remote</a>
                                <br><a button class="btn btn-success" href="../ssh/logthrussh.php?ip=10.10.10.43&dt=ga" >Log</a>
 <br><a button class="btn btn-success" href="../ssh/updatesoft.php?strip=10.10.10.43&dt=ga">UpdateSoftware</a>
<br><a button class="btn btn-danger" href="../ssh/softkill.php?strip=10.10.10.43&dt=ga">Restart Software</a>
<br><a button class="btn btn-danger" href="../ssh/settingcheck.php?strip=10.10.10.43&dt=ga">Check Setting</a>
 </th>
						                </tr>
						              </thead>



						                  <tr><td  valign="top" style="font-size:15px; ";  >
						                     APP VER:

						                  <br><br>
                              HEARTBEAT :

                           <br><br>

						                  <td><div id="redis10_10_10_41"  > </div></td>
						                  <td><div id="redis10_10_10_42"> </div></td>
						                   <td><div id="redis10_10_10_44"> </div></td>
						                    <td><div id="redis10_10_10_45"> </div></td>
                                <td><div id="redis10_10_10_43"> </div></td>


						                  </tr>







						            </table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>



</table>
