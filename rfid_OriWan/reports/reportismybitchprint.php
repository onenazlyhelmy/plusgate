<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <title>Teras Report</title>
    <link rel="icon" type="image/png" href="../img/teras.png"/>
        <!-- CSS File -->
    <link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/pagination.css" rel="stylesheet">

<!---date pick -->
 <link rel="stylesheet" href="../css/jquery-ui.css" />
    
    <!-- Load jQuery JS -->
    <script src="../js/jquery-1.9.1.js"></script>
    <!-- Load jQuery UI Main JS  -->
    <script src="../js/jquery-ui.js"></script>
    
    <!-- Load SCRIPT.JS which will create datepicker for input field  -->
    <script src="../js/script.js"></script>
    
    <link rel="stylesheet" href="../css/runnable.css" />
<!---date pick -->

  </head>
 <body onLoad="prints()">
	 <center><h1>Teras Report</h1>
	 <img src='../img/teras.png'></center>
	 <p><a href ="../log.php" button class="btn btn-success">Back</button></a></p> 
	      

<?php


  include '../function/function_db.php';

  
    $sql_str = $_POST['sql_str'];
   // echo "SQL --> ". $sql_str;
 // die();
 # $data_arr  = 
   	$dbh = Database::connect();
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	$STMsql = $sql_str;

    $STM = $dbh->prepare($STMsql);
    $STM->execute();
    $STMrecords = $STM->fetchAll();

   //$data =  $function_db->reportcsv($sql_str);
	
	echo "<table class='table table-striped table-bordered'>";
	// For Exporting Records to Excel we will send $EntryBy in link and will gate it on ExportToExcel page for stats for this user.	
	echo"<tr><th th colspan=11>Teras RFID Log</div></th></tr>";
	
	#echo"<a href='ExportToExcel.php?val=$EntryBy' target=_blank><img src='img/e2e.png' alt='Export To Excel' border='' class='e2e' /></a>";
	echo"               <tr>
						
						<th>Date Time</th>
						<th>TAGID/EPC</th>
						<!-- <th>raw_userid</th> -->
						<th>Name</th>
						<th>Dept</th>
						<!-- <th>raw_laneid</th> -->
						<th>Status</th>
						<th>Plate No</th>
						<th>Lane</th>
					    </tr>";
   	// We use foreach loop here to echo records.
	foreach($STMrecords as $row)
        {
		      echo "<tr>";
	          #echo '<td>'. $row['idraw_log'] . '</td>';
              echo '<td>'. $row['raw_tarikh'] . '</td>';
              echo '<td>'. $row['raw_tagid'] . '</td>';
             // echo '<td>'. $row['raw_userid'] . '</td>';
              echo '<td>'. $row['raw_name'] . '</td>';
              echo '<td>'. $row['raw_dept'] . '</td>';
              echo '<td>'. $row['raw_status'] . '</td>';
              echo '<td>'. $row['users_plno'] . '</td>';
          #    echo '<td>'. $row['raw_path'] . '</td>';
                switch ($row['raw_path']) {
				    case 'M01':
				        echo '<td> Annex 1 Entry</td>';
				        break;
				    case 'M02':
				         echo '<td> Annex 2 Entry</td>';
				        break;
				    case 'M03':
				         echo '<td> Annex 3 Entry</td>';
				        break;
				    case 'M04':
				         echo '<td> Front Gate Entry</td>';
				        break;
				    case 'M05':
				         echo '<td> Back Gate Entry</td>';
				        break;
				    case 'K01':
				        echo '<td> Annex 1 Exit</td>';
				        break;
				    case 'K02':
				    echo '<td> Annex 2 Exit</td>';
				        break;
				    case 'K03':
				        echo '<td> Annex 3 Exit</td>';
				        break;
				    case 'K04':
				        echo '<td> Front Gate Exit</td>';
				        break;
				    case 'K05':
				        echo '<td> Back Gate Exit</td>';
				        break;
				    
				    default:
				        echo '<td> Annex 100</td>';
				}
	   		#echo "<td>" .$r[8] ."</td>";
	   		#echo "<td>" .$r[9] ."</td>";
	   		#echo "<td>" .$r[10] ."</td>";
			#echo '<td width=250>';
           # echo '<a class="btn btn-mini btn-primary" href="adminread.aspx?id='.$r['idusers'].'">Info</a>';
           # echo '  ';
          #  echo '<a class="btn btn-mini" href="adminupdate.aspx?id='.$r['idusers'].'">Update</a>';
         #   echo '  ';
          #  echo '<a class="btn btn-mini" href="admindelete.aspx?id='.$r['idusers'].'">Delete</a>';
         #   echo '</td>';
 			echo "</tr>";  
		}
	echo "</table>";
	// For showing pagination below the table we will echo $pagination here after </table>. For showing above the table we will echo $pagination before <table>

	// Closing MySQL database connection   
    $dbh = null;
	?>
     <script type="text/javascript">
      function prints() {
      	window.print();
      //	window.location.href = "reportismybitch.php";
      }
     </script>
      
   
		
			
	
  </body>
</html>	
