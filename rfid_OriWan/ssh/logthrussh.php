
<style>
  .btn {
  display: inline-block;
  *display: inline;
  padding: 4px 12px;
  margin-bottom: 0;
  *margin-left: .3em;
  font-size: 14px;
  line-height: 20px;
  color: #333444;
  text-align: center;
  text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
  vertical-align: middle;
  cursor: pointer;
  background-color: #f5f5f5;
  *background-color: #e6e6e6;
  background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6));
  background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);
  background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);
  background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);
  background-repeat: repeat-x;
  border: 1px solid #cccccc;
  *border: 0;
  border-color: #e6e6e6 #e6e6e6 #bfbfbf;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  border-bottom-color: #b3b3b3;
  -webkit-border-radius: 4px;
     -moz-border-radius: 4px;
          border-radius: 4px;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe6e6e6', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
  *zoom: 1;
  -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
     -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
          box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
      }
  .btn-primary.active,
.btn-warning.active,
.btn-danger.active,
.btn-success.active,
.btn-info.active,
.btn-inverse.active {
  color: rgba(255, 255, 255, 0.75);
}

.btn-danger {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #da4f49;
  *background-color: #bd362f;
  background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#bd362f));
  background-image: -webkit-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);
  background-repeat: repeat-x;
  border-color: #bd362f #bd362f #802420;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffbd362f', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.btn-success {
  color: #fff333;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #5bb75b;
  *background-color: #51a351;
  background-image: -moz-linear-gradient(top, #62c462, #51a351);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#62c462), to(#51a351));
  background-image: -webkit-linear-gradient(top, #62c462, #51a351);
  background-image: -o-linear-gradient(top, #62c462, #51a351);
  background-image: linear-gradient(to bottom, #62c462, #51a351);
  background-repeat: repeat-x;
  border-color: #51a351 #51a351 #387038;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462', endColorstr='#ff51a351', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.btn-success:hover,
.btn-success:focus,
.btn-success:active,
.btn-success.active,
.btn-success.disabled,
.btn-success[disabled] {
  color: #ffffff;
  background-color: #51a351;
  *background-color: #499249;
}


.btn-success:active,
.btn-success.active {
  background-color: #408140 \9;
}
button.btn,
input[type="submit"].btn {
  *padding-top: 3px;
  *padding-bottom: 3px;
}
</style>
<?php


include('Net/SSH2.php');
include('../function/function_db.php');

$function_db = new function_db();
    $IParr  = Array();
    $IParr = $function_db->get_piIPlist();

     if( !empty($_REQUEST['ip'])) {
          $ip=$_REQUEST['ip'];
        //  $dtlog = $dt."_ga.txt";
          $strip = $ip;
      } else {
      	// $dt= $date->format('Ymd');
         $strip = $IParr[0];
      }

$ssh = new Net_SSH2($strip);
if (!$ssh->login('root', 'root123')) {
    exit('Login Failed');
}
   $date = new DateTime('now',new DateTimeZone('Singapore'));
   // $tarikh_reg= $date->format('Y-m-d H:i:s');

      $dt="error";
      if( !empty($_REQUEST['dt'])) {

          $dt = $_REQUEST['dt'];
          if($dt=='ga') {
			     $dte= $date->format('Ymd');

                 $dtlog = $dte."_ga.txt";
			  }else if($dt=='error') {
				    $dte= $date->format('Ymd');
                   $dtlog = $dte."_error.txt";
			  } else {
				    $dtlog = $dt.".txt";
				  }

      } else {
      	 $dt= $date->format('Ymd');
         $dtlog = $dt."_ga.txt";
      }
//$ssh = new Net_SSH2('192.168.0.117');
//if (!$ssh->login('root', 'root123')) {
//    exit('Login Failed');
//}

echo "Raspberry(<font color='blue'><b> ".$strip."</b> </font>) PI Date :<font color='red'><b> ".$ssh->exec("date")."</b></font><br>";
echo "<a button class='btn btn-success' href='../hardware/'>Back..(powered by parrot)</a>";

echo "<br>";
echo "<br><font color='green'>";
echo " *****************log file name: <b>".$dtlog."</b>********** log ip :<b>".$strip."</b> ***** gate file type: <b>$dt</b>  ******************************************";

echo "<textarea rows='30' cols='120'>".$ssh->exec("cat /home/pi/rfidGA/GA/log/$dtlog")."</textarea>";
echo "<br> ****************************************************************** END **************************************************";
echo "</font>";





?>
