<?php
include 'database.php';

class function_db {

	private static $pdo = null ;
	
	 #init..dbconn
	 public function __construct() {
        self::$pdo = Database::connect();
    }
   

        public static function getcurr_db() {
		
			
		    return Database::getdb();
		            

		    }

    public static function reg_user($users_name,$users_tagid,$employee_id,$users_department,$tarikh_reg,$users_org) {
		
			
			try {
				
	        $pdo = self::$pdo;
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO users (users_names,users_tagid,employee_id,users_department,tarikh_reg,users_status,users_org) 
			values(?,?,?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($users_name,$users_tagid,$employee_id,$users_department,$tarikh_reg,'OK',$users_org));
			//return "Success update..!!";

			Database::disconnect();
			}
	        	catch(PDOException $e)
				{
					return $e->getMessage();
				}

		    }

		public static function updateusers($users_names,$users_tagid,$employee_id,$idusertag) {
		
			
			try {
				
	        $pdo = self::$pdo;
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE users  set users_names = '$users_names', users_tagid = '$users_tagid', employee_id ='$employee_id' WHERE idusertag = '$idusertag'";
			$q = $pdo->prepare($sql);
			$q->execute();
			return "Success update..!!";

			Database::disconnect();
			}
	        	catch(PDOException $e)
			{
				return $e->getMessage();
			}

		             



		    }
		       public static function graphmon_insert($dt,$k01,$k02,$k03,$k04,$k05,$m01,$m02,$m03,$m04,$m05) {
		
			
			try {
				
	        $pdo = self::$pdo;
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO graphmon (dt,K01,K02,K03,K04,K05,M01,M02,M03,M04,M05) 
			values(?,?,?,?,?,?,?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($dt,$k01,$k02,$k03,$k04,$k05,$m01,$m02,$m03,$m04,$m05));
			//return "Success update..!!";

			Database::disconnect();
			}
	        	catch(PDOException $e)
			{
				return $e->getMessage();
			}

		     
		    }

	  	public static function graphmon_del($id) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")
			
			try {
				
	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "DELETE FROM graphmon";
            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            #$data = $q->fetchall();
			
			return "Delete success !";
			#Database_local::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }
		public static function graphmon_chcklatest($dt) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")
			
			try {
				
	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT count(dt) FROM graphmon WHERE dt='$dt'";
            $q = $pdo->prepare($sql);
            $q->execute();
            $data = $q->fetch(PDO::FETCH_ASSOC);
            #$data = $q->fetchall();
			
			return $data;
			#Database_local::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }

    public static function reg_user1($users_names,$users_tagid,$users_department,$tarikh_reg) {
		
			
			try {
				
	        $pdo = self::$pdo;
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO users (users_names,users_tagid,users_department,tarikh_reg,users_status) 
			values(?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($users_names,$users_tagid,$users_department,$tarikh_reg,'OK'));
			//return "Success update..!!";

			Database::disconnect();
			}
	        	catch(PDOException $e)
			{
				return $e->getMessage();
			}

		     
		    }
	
	
    	public static function getuserlist($id) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")
			
			try {
				
	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT * FROM users WHERE (idusertag='$id' OR users_tagid='$id')";
            $q = $pdo->prepare($sql);
            $q->execute();
            $data = $q->fetch(PDO::FETCH_ASSOC);
            #$data = $q->fetchall();
			
			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }


    	public static function deleteusers($id) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")
			
			try {
				
	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "DELETE FROM users WHERE idusertag='$id'";
            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            #$data = $q->fetchall();
			
			return "success !";
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }

		  public static function reportcsv($datefrom,$dateto,$sql_path) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")
			
			try {
				
	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT * from stardb.raw_log WHERE
             STR_TO_DATE(raw_tarikh,'%d/%m/%Y %H:%i:%s') between STR_TO_DATE('$datefrom 00:00:00', '%d/%m/%Y %H:%i:%s')
             AND STR_TO_DATE('$dateto 23:59:00', '%d/%m/%Y %H:%i:%s') $sql_path
             GROUP BY ROUND(UNIX_TIMESTAMP(STR_TO_DATE(raw_tarikh,'%d/%m/%Y %H:%i:%s'))/40)  ORDER BY raw_tarikh DESC;";

            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            $data = $q->fetchall();
			
			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }
           public static function reportnamecsv($users_names,$sql_path) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")
			
			try {
				
	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT * FROM stardb.raw_log WHERE raw_name like '".$users_names."' ".$sql_path.
              " GROUP BY ROUND(UNIX_TIMESTAMP(STR_TO_DATE(raw_tarikh,'%d/%m/%Y %H:%i:%s'))/40) ORDER BY idraw_log DESC";

            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            $data = $q->fetchall();
			
			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }
		    public static function dummyreportcsv($datefrom,$dateto) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")
			
			try {
				
	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT * from stardb.raw_dummy WHERE aw_name <>'unknown' AND
             STR_TO_DATE(tarikh,'%d/%m/%Y %H:%i:%s') between STR_TO_DATE('$datefrom 00:00:00', '%d/%m/%Y %H:%i:%s')
             AND STR_TO_DATE('$dateto 23:59:00', '%d/%m/%Y %H:%i:%s') ORDER BY tarikh DESC;";

            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            $data = $q->fetchall();
			
			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }

		   public static function chck_employee_id($employee_id) {
			#("user_profile_member","idusers","6","iduser_profile_member","1")
			
			try {

		
				
	    	$pdo = self::$pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT count(employee_id) from users WHERE employee_id='".$employee_id."'";

            $q = $pdo->prepare($sql);
            $q->execute();
           # $data = $q->fetch(PDO::FETCH_ASSOC);
            $data = $q->fetchall();
			
			return $data;
			Database::disconnect();
			}
		catch(PDOException $e)
			{
				return $e->getMessage();
			}

		$pdo = null;

		     }


		     public static function GCMsend($message,$regID) { 
               
               # $message = $_POST['message'];

		        	$apiKey = "AIzaSyA1UlYyaS1h07iyeRvpexwR4p8x0Laik0w";

					// Set POST variables
					$url = 'https://android.googleapis.com/gcm/send';

					$fields = array(
					                'registration_ids'  => array($regID),
					                'data'              => array( "message" => $message ),
					                );

					$headers = array( 
					                    'Authorization: key=' . $apiKey,
					                    'Content-Type: application/json'
					                );

					// Open connection
					$ch = curl_init();

					// Set the url, number of POST vars, POST data
					curl_setopt( $ch, CURLOPT_URL, $url );

					curl_setopt( $ch, CURLOPT_POST, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

					curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );

					// Execute post
					$result = curl_exec($ch);

					        if ($result === FALSE) {
					            die('Curl failed: ' . curl_error($ch));
					        }

					// Close connection
					curl_close($ch);

					echo $result;

		     }
		

}

?>