<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> </title>
<style type="text/css">

	@media print {
        thead {display: table-header-group;}				
    }
	table.tbl1 tr th {font-size: 20px; }
	table.tbl1 tr td {font-size: 9px; }
    table {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px}	
	table.tbl1 {border-collapse: collapse;}
	
	.border {border:1px solid #000000; border-left:1px solid #000000; border-bottom:1px solid #000000; 
}
    
</style>
</head>
 <body onLoad="prints()">
<?php
include '../function/function_db.php';
$timep=date("h:i:s A");
$today=date("d/m/Y");
$sql_str = $_POST['sql_str'];
$dt_frm = $_POST['df'];
$dt_to = $_POST['dt'];
$users_org = $_POST['users_org'];
$rdr_loc = $_POST['rdr_loc'];
$name = $_POST['name'];
$dep = $_POST['dep'];

switch ($rdr_loc) {
	case 'M01':
		$rdr_loc="Annex 1 Entry";
		break;
	case 'M02':
		 $rdr_loc="Annex 2 Entry";
		break;
	case 'M03':
		 $rdr_loc="Annex 3 Entry";
		break;
	case 'M04':
		 $rdr_loc="Front Gate Entry";
		break;
	case 'M05':
		 $rdr_loc="Back Gate Entry";
		break;
	case 'K01':
		$rdr_loc="Annex 1 Exit";
		break;
	case 'K02':
		$rdr_loc="Annex 2 Exit";
		break;
	case 'K03':
		$rdr_loc="Annex 3 Exit";
		break;
	case 'K04':
		$rdr_loc="Front Gate Exit";
		break;
	case 'K05':
		$rdr_loc="Front Gate Exit";
		break;
	case 'ALL':
		$rdr_loc="ALL";
		break;
	default:
		$rdr_loc="Annex 100</td>";
}
				
$dbh = Database::connect();
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$STMsql = $sql_str;
$STM = $dbh->prepare($STMsql);
$STM->execute();
$STMrecords = $STM->fetchAll();
?>

<table class="tbl1">
<thead>
	<tr><th colspan="7">TERAS TEKNOLOGI SDN BHD</th></tr>
    <tr><th colspan="7">**********************************************</th></tr>
    <tr><th colspan="7">RFID Log Report</th></tr>
    <tr><td colspan="4">Date Printed : <?php echo "$today" ?></td><td colspan="3">Time Printed : <?php echo "$timep" ?> </td></tr>
    <tr><td colspan="7">Prepared By : Administrator</td></tr>
    <tr><td colspan="7">Select by : Date: <?php echo "$dt_frm"?> to <?php echo "$dt_to" ?>, Organization: <?php echo "$users_org" ?>,Reader Location: <?php echo "$rdr_loc" ?>, Name: <?php echo "$name" ?>, Department: <?php echo "$dep" ?></td></tr>
    <tr><td colspan="7"></td></tr>
			<tr>
				<td class="border"><strong>Date Time</strong></td>
				<td class="border"><strong>TAGID/EPC</strong></td>
				<td class="border"><strong>Name</strong></td>
                <td class="border"><strong>Department</strong></td>
				<td class="border"><strong>Plate No</strong></td>
				<td class="border"><strong>Status</strong></td>
                <td class="border"><strong>Lane</strong></td>
			</tr>
		</thead>
		<tbody>
<?php
	// We use foreach loop here to echo records.
	foreach($STMrecords as $row)
        {
			echo "<tr>";
	          #echo '<td>'. $row['idraw_log'] . '</td>';
              echo '<td class="border">'. $row['raw_tarikh'] . '</td>';
              echo '<td class="border">'. $row['raw_tagid'] . '</td>';
             // echo '<td>'. $row['raw_userid'] . '</td>';
              echo '<td class="border">'. $row['raw_name'] . '</td>';
              echo '<td class="border">'. $row['raw_dept'] . '</td>';
			  echo '<td class="border">'. $row['users_plno'] . '</td>';
              echo '<td class="border">'. $row['raw_status'] . '</td>';              
          #    echo '<td>'. $row['raw_path'] . '</td>';
                switch ($row['raw_path']) 
				{
				    case 'M01':
				        echo '<td class="border"> Annex 1 Entry</td>';
				        break;
				    case 'M02':
				         echo '<td class="border"> Annex 2 Entry</td>';
				        break;
				    case 'M03':
				         echo '<td class="border"> Annex 3 Entry</td>';
				        break;
				    case 'M04':
				         echo '<td class="border"> Front Gate Entry</td>';
				        break;
				    case 'M05':
				         echo '<td class="border"> Back Gate Entry</td>';
				        break;
				    case 'K01':
				        echo '<td class="border"> Annex 1 Exit</td>';
				        break;
				    case 'K02':
				    echo '<td class="border"> Annex 2 Exit</td>';
				        break;
				    case 'K03':
				        echo '<td class="border"> Annex 3 Exit</td>';
				        break;
				    case 'K04':
				        echo '<td class="border"> Front Gate Exit</td>';
				        break;
				    case 'K05':
				        echo '<td class="border"> Back Gate Exit</td>';
				        break;
				    
				    default:
				        echo '<td class="border"> Annex 100</td>';
				}
					echo "</tr>";  
		}
		// Closing MySQL database connection   
    $dbh = null;
	?>
     <script type="text/javascript">
      function prints() {
      	window.print();
      //	window.location.href = "reportismybitch.php";
      }
     </script>

</table>
</body>
</html>