<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <title>Teras Log</title>
    <link rel="icon" type="image/png" href="img/teras.png"/>
        <!-- CSS File -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/pagination.css" rel="stylesheet">

<!---date pick -->
 <link rel="stylesheet" href="css/jquery-ui.css" />
    
    <!-- Load jQuery JS -->
    <script src="js/jquery-1.9.1.js"></script>
    <!-- Load jQuery UI Main JS  -->
    <script src="js/jquery-ui.js"></script>
    
    <!-- Load SCRIPT.JS which will create datepicker for input field  -->
    <script src="js/script.js"></script>
    
    <link rel="stylesheet" href="css/runnable.css" />
<!---date pick -->

  </head>
 <body>
	 <center><h1>Teras RFID Log -Performace test on EXIT PATH annex 3</h1>
	 <img src='img/teras.png'></center>
	   <br>
	   
	   <p><a href ="create.php" button class="btn btn-success">List User/Create</button></a>
	   <a href ="index.php" button class="btn btn-success">Go to log/report</a></p>

	   				
  <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
  <script>
			function suggest(inputString){
			if(inputString.length == 0) {
			$('#suggestions').fadeOut();
			} else {
				$.ajax({
				url: "function/autosuggestname",
				data: 'act=autoSuggestUser&queryString='+inputString,
				success: function(msg){
					if(msg.length >0) {
					$('#suggestions').fadeIn();
					$('#suggestionsList').html(msg);
					//$('#country').removeClass('load');
					}
				  }
				 });
				}
			  }
			function fill(thisValue) {
				$('#name').val(thisValue);
				setTimeout("$('#suggestionsname').fadeOut();", 600);
			}
			function fillId(thisValue) {
				$('#iduser_profile').val(thisValue);
				$('#id_url').html('<a class="btn btn-mini"  href="adminupdate.aspx?id='+thisValue+'">Update</a><a class="btn btn-mini"  href="adminread.aspx?id='+thisValue+'">Info</a>');
				setTimeout("$('#suggestions').fadeOut();", 600);
			}

			//---------------------------------search school name of user id
			function suggestschoolname(inputString){
			if(inputString.length == 0) {
			$('#suggestions').fadeOut();
			} else {
				$.ajax({
				url: "function/autosuggestsch_name",
				data: 'act=autoSuggestUser&queryString='+inputString,
				success: function(msg){
					if(msg.length >0) {
					$('#suggestionsname').fadeIn();
					$('#suggestionsListname').html(msg);
					//$('#country').removeClass('load');
					}
				  }
				 });
				}
			  }
			function fillname(thisValue) {
				$('#school_name').val(thisValue);
				setTimeout("$('#suggestionsname').fadeOut();", 600);
			}
			function fillIdname(thisValue) {
				$('#nameuser').val(thisValue);
				setTimeout("$('#suggestionsname').fadeOut();", 600);
			}
			function filluserid(thisValue,schoolnameval) {
				$('#userid').val(thisValue);
				$('#id_urlname').html('<a class="btn btn-mini"  href="userextend.aspx?id='+thisValue+'&sn='+schoolnameval+'">Extend</a>');
				setTimeout("$('#suggestionsname').fadeOut();", 600);
			}
				function alerts(obj) {
			     //  alert("Date :"+obj);
			}
			</script> 
             
             <form action="dummyreport.php" method="get">
			 <p>From: <input type="text" id="datefrom" name="datefrom" /> To  : <input type="text" id="dateto" name="dateto" />
		   	 &nbsp&nbsp<input type="submit" button class="btn btn-success" value="Report" onClick=''></p>
			 </form> 
             
  			 
<?php 
     include 'function/function_db.php';
	#try { $dbh = new PDO("mysql:host=$hostname;dbname=sync", $username, $password); }
	// In case of error PDO exception will show error message.
	#catch(PDOException $e) {    echo $e->getMessage();    }
	
	$dbh = Database::connect();
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	$adjacents = 2;
	
#	$EntryBy = "furqan.aziz";
	
    #$STM = $dbh->prepare("SELECT ServerName FROM statstracker WHERE EntryBy = :EntryBy");
    $STM = $dbh->prepare("SELECT * FROM raw_dummy WHERE raw_name <>'unknown' ORDER BY idraw_dummy DESC");
	
   # $STM->bindParam(':EntryBy', $EntryBy);
	
    $STM->execute();
	// Count no. of records	
	$Records = $STM->rowCount();
	
	$targetpage = "dummy.php";
	
	$limit = 10; 
	if (empty($_GET['page'])) {
	  $page = 1;
	}  else {
	$page = $_GET['page'];
	
	}
	if($page) 
	//First Item to dipaly on this page
		$start = ($page - 1) * $limit; 			
	else
	//if no page variable is given, set start to 0
		$start = 0;	
    
   		
	
	#$STM2 = $dbh->prepare("SELECT `SrNo`, `ServerName`, `HiMemUti`, `AvgMemUti`, `HiCpuUti`, `AvgCpuUti`, `HiIOPerSec`, `AvgIOPerSec`, `HiDiskUsage`, `AvgDsikUsage`, `EntryBy` FROM statstracker WHERE EntryBy = :EntryBy ORDER BY SrNo LIMIT $start, $limit");
	$STM2 = $dbh->prepare("SELECT * FROM raw_dummy WHERE raw_name <>'unknown' ORDER BY idraw_dummy DESC LIMIT $start, $limit");
	// bind paramenters, Named paramenters alaways start with colon(:)
    #$STM2->bindParam(':EntryBy', $EntryBy);
	// For Executing prepared statement we will use below function
    $STM2->execute();
	// We will fetch records like this and use foreach loop to show multiple Results later in bottom of the page.
	 $STMrecords = $STM2->fetchAll();
	// Setup page variables for display. If no page variable is given, default to 1.
	if ($page == 0) $page = 1;
	//previous page is page - 1					
	$prev = $page - 1;
	//next page is page + 1						
	$next = $page + 1;
	//lastpage is = total Records / items per page, rounded up.							
	$lastpage = ceil($Records/$limit);
	//last page minus 1	
	$lpm1 = $lastpage - 1;						
	//Now we apply our rules and draw the pagination object. We're actually saving the code to a variable in case we want to draw it more than once.
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class='pagination'>";
		//previous button
		if ($page > 1) 
			$pagination.= "<a href='$targetpage?page=$prev'>Previous</a>";
		else
			$pagination.= "<span class='disabled'>Previous</span>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class='current'>$counter</span>";
				else
					$pagination.= "<a href='$targetpage?page=$counter'>$counter</a>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class='current'>$counter</span>";
					else
						$pagination.= "<a href='$targetpage?page=$counter'>$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href='$targetpage?page=$lpm1'>$lpm1</a>";
				$pagination.= "<a href='$targetpage?page=$lastpage'>$lastpage</a>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href='$targetpage?page=1'>1</a>";
				$pagination.= "<a href='$targetpage?page=2'>2</a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class='current'>$counter</span>";
					else
						$pagination.= "<a href='$targetpage?page=$counter'>$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href='$targetpage?page=$lpm1'>$lpm1</a>";
				$pagination.= "<a href='$targetpage?page=$lastpage'>$lastpage</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href='$targetpage?page=1'>1</a>";
				$pagination.= "<a href='$targetpage?page=2'>2</a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class='current'>$counter</span>";
					else
						$pagination.= "<a href='$targetpage?page=$counter'>$counter</a>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<a href='$targetpage?page=$next'>Next</a>";
		else
			$pagination.= "<span class='disabled'>Next</span>";
		$pagination.= "</div>\n";		
	}
	//Below is a start of table in which we will show records using foreach loop.
	
	echo "<table class='table table-striped table-bordered'>";
	// For Exporting Records to Excel we will send $EntryBy in link and will gate it on ExportToExcel page for stats for this user.	
	echo"<tr><th th colspan=11>Teras RFID Log</div></th></tr>";
	
	#echo"<a href='ExportToExcel.php?val=$EntryBy' target=_blank><img src='img/e2e.png' alt='Export To Excel' border='' class='e2e' /></a>";
	echo"               <tr>
						<th>id</th>
						<th>tagid</th>
						<th>name</th>
						<th>tarikh</th>
						
					    </tr>";
   	// We use foreach loop here to echo records.
	foreach($STMrecords as $row)
        {
		      echo "<tr>";
	          echo '<td>'. $row['idraw_dummy'] . '</td>';
              echo '<td>'. $row['raw_log'] . '</td>';
              echo '<td>'. $row['raw_name'] . '</td>';
              echo '<td>'. $row['tarikh'] . '</td>';
             
	   		#echo "<td>" .$r[8] ."</td>";
	   		#echo "<td>" .$r[9] ."</td>";
	   		#echo "<td>" .$r[10] ."</td>";
			#echo '<td width=250>';
           # echo '<a class="btn btn-mini btn-primary" href="adminread.aspx?id='.$r['idusers'].'">Info</a>';
           # echo '  ';
          #  echo '<a class="btn btn-mini" href="adminupdate.aspx?id='.$r['idusers'].'">Update</a>';
         #   echo '  ';
          #  echo '<a class="btn btn-mini" href="admindelete.aspx?id='.$r['idusers'].'">Delete</a>';
         #   echo '</td>';
 			echo "</tr>";  
		}
	echo "</table>";
	// For showing pagination below the table we will echo $pagination here after </table>. For showing above the table we will echo $pagination before <table>
	echo $pagination;
	// Closing MySQL database connection   
    $dbh = null;
	?>
  </body>
</html>	
