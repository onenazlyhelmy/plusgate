
<html lang="en">
	<head>
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/pagination.css" rel="stylesheet">

		<title>Teras Log</title>
		<link rel="icon" type="image/png" href="img/teras.png"/>
        <!--10/01/2019 arie add font-awesome-->
		<link href="assets/fontawesome/css/all.css" rel="stylesheet" />
		<link href="assets/fontawesome/css/fontawesome.css" rel="stylesheet">
		<link href="assets/fontawesome/css/brands.css" rel="stylesheet">
		<link href="assets/fontawesome/css/solid.css" rel="stylesheet">
	</head>

<body bgcolor="#FFFFFF">

<?php
	// 08-01-2019: arie add :refer file from redisstatus folder 
	
	//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	//^^^^^^^^^^^  STRT READ DATA FROM TEXT FILE ^^^^^^^^^^^
	//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  
	//^^^^^^^^^^^^^^^^^^^^ START READ LANE 1 ^^^^^^^^^^^^^^^^^^^^
	$myfile = fopen("redisstatus/input01.txt", "r") or die("Unable to open file!");
	$input01= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/output01.txt", "r") or die("Unable to open file!");
	$output01= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/mfgno201.txt", "r") or die("Unable to open file!");
	$mfgno201= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/mfgno101.txt", "r") or die("Unable to open file!");
	$mfgno101= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/date_mfgno101.txt", "r") or die("Unable to open file!");
	$date_mfgno101= fgets($myfile);

	fclose($myfile);
	$myfile = fopen("redisstatus/date_mfgno201.txt", "r") or die("Unable to open file!");
	$date_mfgno201= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/op_xport01.txt", "r") or die("Unable to open file!");
	$op_xport01= fgets($myfile);	
	fclose($myfile);

	$myfile = fopen("redisstatus/epc_dat_ent01.txt", "r") or die("Unable to open file!");
	$epc_dat_ent01= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/date_park_ent01.txt", "r") or die("Unable to open file!");
	$date_park_ent01= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/epc_dat_ext01.txt", "r") or die("Unable to open file!");
	$epc_dat_ext01= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/date_park_ext01.txt", "r") or die("Unable to open file!");
	$date_park_ext01= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/app.rfid_ent01.txt", "r") or die("Unable to open file!");
	$rfid_ent01= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/app.rfid_ext01.txt", "r") or die("Unable to open file!");
	$rfid_ext01= fgets($myfile);
	fclose($myfile);
	//^^^^^^^^^^^^^^^^^^^^ END READ LANE 1 ^^^^^^^^^^^^^^^^^^^^
	
	//^^^^^^^^^^^^^^^^^^^^ START READ LANE 2 ^^^^^^^^^^^^^^^^^^^^
	$myfile = fopen("redisstatus/input02.txt", "r") or die("Unable to open file!");
	$input012= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/output02.txt", "r") or die("Unable to open file!");
	$output012= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/mfgno202.txt", "r") or die("Unable to open file!");
	$mfgno2012= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/mfgno102.txt", "r") or die("Unable to open file!");
	$mfgno1012= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/date_mfgno102.txt", "r") or die("Unable to open file!");
	$date_mfgno1012= fgets($myfile);

	fclose($myfile);
	$myfile = fopen("redisstatus/date_mfgno202.txt", "r") or die("Unable to open file!");
	$date_mfgno2012= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/op_xport02.txt", "r") or die("Unable to open file!");
	$op_xport012= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/epc_dat_ent02.txt", "r") or die("Unable to open file!");
	$epc_dat_ent012= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/date_park_ent02.txt", "r") or die("Unable to open file!");
	$date_park_ent012= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/epc_dat_ext02.txt", "r") or die("Unable to open file!");
	$epc_dat_ext012= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/date_park_ext02.txt", "r") or die("Unable to open file!");
	$date_park_ext012= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/app.rfid_ent02.txt", "r") or die("Unable to open file!");
	$rfid_ent012= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/app.rfid_ext02.txt", "r") or die("Unable to open file!");
	$rfid_ext012= fgets($myfile);
	fclose($myfile);
	//^^^^^^^^^^^^^^^^^^^^ END READ LANE 2 ^^^^^^^^^^^^^^^^^^^^
	
	
	//^^^^^^^^^^^^^^^^^^^^ START READ LANE 3 ^^^^^^^^^^^^^^^^^^^^
	$myfile = fopen("redisstatus/input04.txt", "r") or die("Unable to open file!");
	$input013= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/output04.txt", "r") or die("Unable to open file!");
	$output013= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/mfgno204.txt", "r") or die("Unable to open file!");
	$mfgno2013= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/mfgno104.txt", "r") or die("Unable to open file!");
	$mfgno1013= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/date_mfgno104.txt", "r") or die("Unable to open file!");
	$date_mfgno1013= fgets($myfile);

	fclose($myfile);
	$myfile = fopen("redisstatus/date_mfgno204.txt", "r") or die("Unable to open file!");
	$date_mfgno2013= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/op_xport04.txt", "r") or die("Unable to open file!");
	$op_xport013= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/epc_dat_ent04.txt", "r") or die("Unable to open file!");
	$epc_dat_ent013= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/date_park_ent04.txt", "r") or die("Unable to open file!");
	$date_park_ent013= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/epc_dat_ext04.txt", "r") or die("Unable to open file!");
	$epc_dat_ext013= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/date_park_ext04.txt", "r") or die("Unable to open file!");
	$date_park_ext013= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/app.rfid_ent04.txt", "r") or die("Unable to open file!");
	$rfid_ent013= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/app.rfid_ext04.txt", "r") or die("Unable to open file!");
	$rfid_ext013= fgets($myfile);
	fclose($myfile);
	//^^^^^^^^^^^ END READ LANE 3 ^^^^^^^^^^^
	
	//^^^^^^^^^^^ START  READ LANE 4 ^^^^^^^^^^^
	$myfile = fopen("redisstatus/input05.txt", "r") or die("Unable to open file!");
	$input014= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/output05.txt", "r") or die("Unable to open file!");
	$output014= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/mfgno205.txt", "r") or die("Unable to open file!");
	$mfgno2014= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/mfgno105.txt", "r") or die("Unable to open file!");
	$mfgno1014= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/date_mfgno105.txt", "r") or die("Unable to open file!");
	$date_mfgno1014= fgets($myfile);

	fclose($myfile);
	$myfile = fopen("redisstatus/date_mfgno205.txt", "r") or die("Unable to open file!");
	$date_mfgno2014= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/op_xport05.txt", "r") or die("Unable to open file!");
	$op_xport014= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/epc_dat_ent05.txt", "r") or die("Unable to open file!");
	$epc_dat_ent014= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/date_park_ent05.txt", "r") or die("Unable to open file!");
	$date_park_ent014= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/epc_dat_ext05.txt", "r") or die("Unable to open file!");
	$epc_dat_ext014= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/date_park_ext05.txt", "r") or die("Unable to open file!");
	$date_park_ext014= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/app.rfid_ent05.txt", "r") or die("Unable to open file!");
	$rfid_ent014= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/app.rfid_ext05.txt", "r") or die("Unable to open file!");
	$rfid_ext014= fgets($myfile);
	fclose($myfile);
	
	//^^^^^^^^^^^ END READ LANE 4 ^^^^^^^^^^^
	
	//^^^^^^^^^^^ START READ LANE 5 ^^^^^^^^^^^
	
	$myfile = fopen("redisstatus/input03.txt", "r") or die("Unable to open file!");
	$input015= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/output03.txt", "r") or die("Unable to open file!");
	$output015= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/mfgno203.txt", "r") or die("Unable to open file!");
	$mfgno2015= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/mfgno103.txt", "r") or die("Unable to open file!");
	$mfgno1015= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/date_mfgno103.txt", "r") or die("Unable to open file!");
	$date_mfgno1015= fgets($myfile);

	fclose($myfile);
	$myfile = fopen("redisstatus/date_mfgno203.txt", "r") or die("Unable to open file!");
	$date_mfgno2015= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/op_xport03.txt", "r") or die("Unable to open file!");
	$op_xport015= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/epc_dat_ent03.txt", "r") or die("Unable to open file!");
	$epc_dat_ent015= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/date_park_ent03.txt", "r") or die("Unable to open file!");
	$date_park_ent015= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/epc_dat_ext03.txt", "r") or die("Unable to open file!");
	$epc_dat_ext015= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/date_park_ext03.txt", "r") or die("Unable to open file!");
	$date_park_ext015= fgets($myfile);
	fclose($myfile);

	$myfile = fopen("redisstatus/app.rfid_ent03.txt", "r") or die("Unable to open file!");
	$rfid_ent015= fgets($myfile);
	fclose($myfile);
	$myfile = fopen("redisstatus/app.rfid_ext03.txt", "r") or die("Unable to open file!");
	$rfid_ext015= fgets($myfile);
	fclose($myfile);
	
	//^^^^^^^^^^^ END READ LANE 5 ^^^^^^^^^^^
	
	
	//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	//^^^^^^^^^^^  END READ DATA FROM TEXT FILE ^^^^^^^^^^^
	//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	
	

    include 'ss/ss.php';

    echo '<table style="width:100%;text-align:center;background-color:#CDEDF9;" border=0 class="table table-striped "> ';
			
	//start top menu			
	echo '<tr>';
	echo '<td><font color="545151"><b> LOCATION </font></td>';
	echo '<td><font color="545151"><b> RFID ENTRY STATUS </font></td>';
	echo '<td><font color="545151"><b> RFID EXIT STATUS </font></td>';
	echo '<td><font color="545151"><b> LOOP1 ENTRY</font> </td>';
	echo '<td><font color="545151"><b> LOOP2 ENTRY</font> </td>';
	echo '<td><font color="545151"><b> LOOP1 EXIT</font> </td>';
	echo '<td><font color="545151"><b> LOOP2 EXIT</font> </td>';
	echo '<td><font color="545151"><b> ENTRY ALB </font> </td>';
	echo '<td><font color="545151"><b> EXIT ALB </font> </td>';
	echo '<td><font color="545151"><b> RFID ENTRY EPC </font></td>';
	echo '<td><font color="545151"><b> RFID EXIT EPC </font></td>';
	echo '<td><font color="545151"><b> MFG ENTRY NO </font></td>';
	echo '<td><font color="545151"><b> MFG EXIT NO </font></td>';
	echo '<td><font color="545151"><b> HEARTBEAT </font></td>';
	echo '</tr>';
	
	//end top menu
	    
	
	
	
	
	
	//************************************************************************************
	//**********************   START DISPLAY AREA ****************************************
	//************************************************************************************
	
	
	//********************** START DISPLAY LANE 1 **********************
	echo '<tr><td><font color="545151"><b> PLUS TOWER </font></td> ';
	echo '<td>';
	
	if($rfid_ent01 =='1' )
		{       
			echo '<i class="far fa-lightbulb" style="font-size:30px;color:green;"></i>';		
		} 
		else 
		{
			echo '<i class="far fa-lightbulb" style="font-size:30px;color:red;"></i>';  
		}
	echo '</td>';
	
	//RFID EXIT STATUS	
	echo '<td>';
	if($rfid_ext01=='1' )
	{       
	echo '<i class="far fa-lightbulb" style="font-size:30px;color:green;"></i>';
	} 
	else 
	{
	echo '<i class="far fa-lightbulb" style="font-size:30px;color:red;"></i>';
	}
    echo '</td>';
   //------------------------------------------sensor------------
   
     //LOOP1 ENTRY
	 
	 if(substr($input01,-5,1)== '1')  {
      $loop1_ent = "blue";      
	  $loop1str_ent = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';
     } else {
      $loop1_ent = "red";      
	  $loop1str_ent = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';
     }
    
	
	//LOOP2 ENTRY 	
	if(substr($input01,-6,1)== '1')  
	{
		$loop2_ent = "blue";      
		$loop2str_ent = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';
	} else 
	{
		$loop2_ent = "red";      
		$loop2str_ent = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';
	}
	echo '</td>';
	
	//LOOP1 EXIT 
	
	if(substr($input01,-11,1)== '1')  
	{
		$loop1_ext = "blue";
		$loop1str_ext = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
	} else 
	{
		$loop1_ext = "red";
		$loop1str_ext = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
    }  
	
	
	//LOOP2 EXIT 	
	if(substr($input01,-12,1)== '1')  
	{
		$loop2_ext = "blue";
		$loop2str_ext = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
	} else 
	{
		$loop2_ext = "red";
		$loop2str_ext = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
	}
	
	
	//ENTRY ALB
	if(substr($output01,-4,1)== '1')  
	{
		$alb_ent = "blue";
		$albstr_ent = "OPEN";
	} else 
	{
		$alb_ent = "red";
		$albstr_ent = "CLOSE";
	}

	//EXIT ALB
	if(substr($output01,-8,1)== '1')  
	{
		$alb_ext = "blue";
		$albstr_ext = "OPEN";
	} else 
	{
		$alb_ext = "red";
		$albstr_ext = "CLOSE";
	}

	echo '<td> <font color= '.$loop1_ent.' size=3>'.$loop1str_ent.'</font></td>';
	echo '<td><font color='.$loop2_ent.' size=3>'.$loop2str_ent.'</font></td>'; 
	echo '<td><font color='.$loop1_ext.' size=3>'.$loop1str_ext.'</font></td>';
	echo '<td><font color='.$loop2_ext.' size=3>'.$loop2str_ext.'</font></td>'; 
	echo '<td> <font color='.$alb_ent .' size=3>'.$albstr_ent .'</font></td>';
	echo '<td> <font color='.$alb_ext .' size=3>'.$albstr_ext .'</font></td>';
	//------------------------------------------------------------------------
	
	//Value MFG no & datetime
	echo '<td><font color=green size=2>'.$epc_dat_ent01.'</font> <br><font color=green size=2>'.$date_park_ent01.'</font></td>';
	echo '<td><font color=blue size=2>'.$epc_dat_ext01.'</font> <br><font color=blue size=2>'.$date_park_ext01.'</font></td>';
	echo '<td><font color=green size=2>'.$mfgno101.'</font> <br><font color=green size=2>'.$date_mfgno101.'</font></td>';     
	echo '<td><font color=blue size=2>'.$mfgno201.'</font> <br><font color=blue size=2>'.$date_mfgno201.'</font></td>';
	
		
	//status alert
	if($op_xport01 == '0') 
		{
			echo '<td><i class="fas fa-heart" style="font-size:25px;color:red;"></i></td>';
		} 
	elseif ($op_xport01 == '1')
		{
			echo '<td><i class="fas fa-heart" style="font-size:35px;color:red;"></i></td>';	
		}
	elseif ($op_xport01 == '2')
		{
			echo '<td><i class="fas fa-heart" style="font-size:30px;color:red;"></i></td>';	
		}	
	
	echo '</tr>';
	
	//********************** END DISPLAY LANE 1 ************************
	
	//********************** START DISPLAY LANE 2 **********************
	echo '<tr><td><font color="545151"><b> ANNEX 2</font></td> ';	
	echo '<td>';
	
    if($rfid_ent012 =='1' )
      {        
		echo '<i class="far fa-lightbulb" style="font-size:30px;color:green;"></i>';		
  } else {    
	echo '<i class="far fa-lightbulb" style="font-size:30px;color:red;"></i>';
  }  
	echo '</td>';
  
  echo '<td>';
    if($rfid_ext012=='1' )
      {   
		echo '<i class="far fa-lightbulb" style="font-size:30px;color:green;"></i>';		
  } else {    
	echo '<i class="far fa-lightbulb" style="font-size:30px;color:red;"></i>';		
  }
   echo '</td>';
   
   //------------------------------------------sensor------------
   
     if(substr($input012,-5,1)== '1')  {
      $loop1_ent2 = "blue";      
	  $loop1str_ent2 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
     } else {
      $loop1_ent2 = "red";      
	  $loop1str_ent2 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
     }
       if(substr($input012,-6,1)== '1')  {
      $loop2_ent2 = "blue";      
	  $loop2str_ent2 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
     } else {
      $loop2_ent2 = "red";      
	  $loop2str_ent2 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
     }
       if(substr($input012,-11,1)== '1')  {
      $loop1_ext2 = "blue";
      $loop1str_ext2 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
     } else {
      $loop1_ext2 = "red";
      $loop1str_ext2 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		

     }  if(substr($input012,-12,1)== '1')  {
      $loop2_ext2 = "blue";
      $loop2str_ext2 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
     } else {
      $loop2_ext2 = "red";
      $loop2str_ext2 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
     }


     if(substr($output012,-4,1)== '1')  {
      $alb_ent2 = "blue";
      $albstr_ent2 = "OPEN";
     } else {
      $alb_ent2 = "red";
      $albstr_ent2 = "CLOSE";
     }

      if(substr($output012,-8,1)== '1')  {
      $alb_ext2 = "blue";
      $albstr_ext2 = "OPEN";
     } else {
      $alb_ext2 = "red";
      $albstr_ext2 = "CLOSE";
     }

	echo '<td> <font color= '.$loop1_ent2.' size=3>'.$loop1str_ent2.'</font></td>';
	echo '<td><font color='.$loop2_ent2.' size=3>'.$loop2str_ent2.'</font></td>'; 
	echo '<td><font color='.$loop1_ext2.' size=3>'.$loop1str_ext2.'</font></td>';
	echo '<td><font color='.$loop2_ext2.' size=3>'.$loop2str_ext2.'</font></td>'; 
	echo '<td><font color='.$alb_ent2 .' size=3>'.$albstr_ent2 .'</font></td>';
	echo '<td><font color='.$alb_ext .' size=3>'.$albstr_ext2 .'</font></td>';
	
	echo '<td><font color=green size=2>'.$epc_dat_ent012.'</font> <br><font color=green size=2>'.$date_park_ent012.'</font></td>';
	echo '<td><font color=blue size=2>'.$epc_dat_ext012.'</font> <br><font color=blue size=2>'.$date_park_ext012.'</font></td>';
	echo '<td><font color=green size=2>'.$mfgno1012.'</font> <br><font color=green size=2>'.$date_mfgno1012.'</font></td>';     
	echo '<td><font color=blue size=2>'.$mfgno2012.'</font> <br><font color=blue size=2>'.$date_mfgno2012.'</font></td>';
	      
	
	//status alert
	if($op_xport012 == '0') 
		{
			echo '<td><i class="fas fa-heart" style="font-size:25px;color:red;"></i></td>';
		} 
	elseif ($op_xport012 == '1')
		{
			echo '<td><i class="fas fa-heart" style="font-size:35px;color:red;"></i></td>';	
		}
	elseif ($op_xport012 == '2')
		{
			echo '<td><i class="fas fa-heart" style="font-size:30px;color:red;"></i></td>';	
		}	
	
		echo '</tr>';
	//********************** END DISPLAY LANE 2 **********************
	
	//********************** START DISPLAY LANE 3 **********************
	echo '<tr><td ><font color="545151"><b> POST 1 </font></td> ';	
	echo '<td>';

	if($rfid_ent013 =='1' )
	{
	echo '<i class="far fa-lightbulb" style="font-size:30px;color:green;"></i>';		
	} else {
	echo '<i class="far fa-lightbulb" style="font-size:30px;color:red;"></i>';		
	}
	echo '</td>';

	echo '<td>';
	if($rfid_ext013=='1' )
	{       
	echo '<i class="far fa-lightbulb" style="font-size:30px;color:green;"></i>';
	} 
	else 
	{
	echo '<i class="far fa-lightbulb" style="font-size:30px;color:red;"></i>';
	}
	echo '</td>';
	//------------------------------------------sensor------------

	if(substr($input013,-5,1)== '1')  {
	$loop1_ent3 = "blue";	
	$loop1str_ent3 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
	} else {
	$loop1_ent3 = "red";	
	$loop1str_ent3 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
	}
	if(substr($input013,-6,1)== '1')  {
	$loop2_ent3 = "blue";	
	$loop2str_ent3 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
	} else {
	$loop2_ent3 = "red";	
	$loop2str_ent3 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
	}
	if(substr($input013,-11,1)== '1')  {
	$loop1_ext3 = "blue";
	$loop1str_ext3 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
	} else {
	$loop1_ext3 = "red";
	$loop1str_ext3 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		

	}  if(substr($input013,-12,1)== '1')  {
	$loop2_ext3 = "blue";
	$loop2str_ext3 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
	} else {
	$loop2_ext3 = "red";
	$loop2str_ext3 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
	}

	if(substr($output013,-4,1)== '1')  {
	$alb_ent3 = "blue";
	$albstr_ent3 = "OPEN";
	} else {
	$alb_ent3 = "red";
	$albstr_ent3 = "CLOSE";
	}

	if(substr($output013,-8,1)== '1')  {
	$alb_ext3 = "blue";
	$albstr_ext3 = "OPEN";
	} else {
	$alb_ext3 = "red";
	$albstr_ext3 = "CLOSE";
	}

	echo '<td> <font color= '.$loop1_ent3.' size=3>'.$loop1str_ent3.'</font></td>';
	echo '<td><font color='.$loop2_ent3.' size=3>'.$loop2str_ent3.'</font></td>';	
	echo '<td><font color='.$loop1_ext3.' size=3>'.$loop1str_ext3.'</font></td>';
	echo '<td><font color='.$loop2_ext3.' size=3>'.$loop2str_ext3.'</font></td>';	
	echo '<td><font color='.$alb_ent3 .' size=3>'.$albstr_ent3 .'</font></td>';
	echo '<td><font color='.$alb_ext3 .' size=3>'.$albstr_ext3 .'</font></td>';
	
	echo '<td><font color=green size=2>'.$epc_dat_ent013.'</font> <br><font color=green size=2>'.$date_park_ent013.'</font></td>';
	echo '<td><font color=blue size=2>'.$epc_dat_ext013.'</font> <br><font color=blue size=2>'.$date_park_ext013.'</font></td>';
	echo '<td><font color=green size=2>'.$mfgno1013.'</font> <br><font color=green size=2>'.$date_mfgno1013.'</font></td>';	
	echo '<td><font color=blue size=2>'.$mfgno2013.'</font> <br><font color=blue size=2>'.$date_mfgno2013.'</font></td>';

	//status alert HEARTBEAT
	if($op_xport013 == '0') 
	{
	echo '<td><i class="fas fa-heart" style="font-size:25px;color:red;"></i></td>';
	} 
	elseif ($op_xport013 == '1')
	{
	echo '<td><i class="fas fa-heart" style="font-size:35px;color:red;"></i></td>';	
	}
	elseif ($op_xport013 == '2')
	{
	echo '<td><i class="fas fa-heart" style="font-size:30px;color:red;"></i></td>';	
	}	

	echo '</tr>';
	
	//********************** END DISPLAY LANE 3 **********************
	
	//********************** START DISPLAY LANE 4 **********************
	
	echo '<tr><td><font color="545151"><b> POST 2 </font></td> ';
	echo '<td>';
    if($rfid_ent014 =='1' )
      {
        echo '<i class="far fa-lightbulb" style="font-size:30px;color:green;"></i>';	
  } else {
    echo '<i class="far fa-lightbulb" style="font-size:30px;color:red;"></i>';	
  }
	echo '</td>';
 
	echo '<td>';
    if($rfid_ext014=='1' )
      {
        echo '<i class="far fa-lightbulb" style="font-size:30px;color:green;"></i>';	
  } else {
    echo '<i class="far fa-lightbulb" style="font-size:30px;color:red;"></i>';	
  }
   echo '</td>';
   
   //------------------------------------------sensor------------
     if(substr($input014,-5,1)== '1')  {
      $loop1_ent4 = "blue";
      $loop1str_ent4 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
     } else {
      $loop1_ent4 = "red";
      $loop1str_ent4 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
     }
       if(substr($input014,-6,1)== '1')  {
      $loop2_ent4 = "blue";
      $loop2str_ent4 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
     } else {
      $loop2_ent4 = "red";
      $loop2str_ent4 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
     }
       if(substr($input014,-11,1)== '1')  {
      $loop1_ext4 = "blue";
      $loop1str_ext4 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
     } else {
      $loop1_ext4 = "red";
      $loop1str_ext4 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		

     }  if(substr($input014,-12,1)== '1')  {
      $loop2_ext4 = "blue";
      $loop2str_ext4 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
     } else {
      $loop2_ext4 = "red";
      $loop2str_ext4 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
     }

     if(substr($output014,-4,1)== '1')  {
      $alb_ent4 = "blue";
      $albstr_ent4 = "OPEN";
     } else {
      $alb_ent4 = "red";
      $albstr_ent4 = "CLOSE";
     }

      if(substr($output014,-8,1)== '1')  {
      $alb_ext4 = "blue";
      $albstr_ext4 = "OPEN";
     } else {
      $alb_ext4 = "red";
      $albstr_ext4 = "CLOSE";
     }

	echo '<td> <font color= '.$loop1_ent4.' size=3>'.$loop1str_ent4.'</font></td>';
	echo '<td><font color='.$loop2_ent4.' size=3>'.$loop2str_ent4.'</font></td>';	
	echo '<td><font color='.$loop1_ext4.' size=3>'.$loop1str_ext4.'</font></td>';
	echo '<td><font color='.$loop2_ext4.' size=3>'.$loop2str_ext4.'</font></td>';	
	echo '<td> <font color='.$alb_ent4 .' size=3>'.$albstr_ent4 .'</font></td>';
	echo '<td> <font color='.$alb_ext4 .' size=3>'.$albstr_ext4 .'</font></td>';
	
	echo '<td><font color=green size=2>'.$epc_dat_ent014.'</font> <br><font color=green size=2>'.$date_park_ent014.'</font></td>';
	echo '<td><font color=blue size=2>'.$epc_dat_ext014.'</font> <br><font color=blue size=2>'.$date_park_ext014.'</font></td>';
	echo '<td><font color=green size=2>'.$mfgno1014.'</font> <br><font color=green size=2>'.$date_mfgno1014.'</font></td>';	
	echo '<td><font color=blue size=2>'.$mfgno2014.'</font> <br><font color=blue size=2>'.$date_mfgno2014.'</font></td>';
		
	//status alert HEARTBEAT
	
	if($op_xport014 == '0') 
		{
			echo '<td><i class="fas fa-heart" style="font-size:25px;color:red;"></i></td>';
		} 
	elseif ($op_xport014 == '1')
		{
			echo '<td><i class="fas fa-heart" style="font-size:35px;color:red;"></i></td>';	
		}
	elseif ($op_xport014 == '2')
		{
			echo '<td><i class="fas fa-heart" style="font-size:30px;color:red;"></i></td>';	
		}	
	echo '</tr>';
	
	//********************** END DISPLAY LANE 4 **********************
	
	//********************** START DISPLAY LANE 5 **********************
	
	echo '<tr><td><font color="545151"><b> ANNEX 3 </font></td> ';
	echo '<td>';
	
    if($rfid_ent015 =='1' )
      {
        echo '<i class="far fa-lightbulb" style="font-size:30px;color:green;"></i>';	
  } else {
    echo '<i class="far fa-lightbulb" style="font-size:30px;color:red;"></i>';	
  }
	echo '</td>';
 
	echo '<td>';
    if($rfid_ext015=='1' )
      {
        echo '<i class="far fa-lightbulb" style="font-size:30px;color:green;"></i>';	
  } else {
    echo '<i class="far fa-lightbulb" style="font-size:30px;color:red;"></i>';	
  }
   echo '</td>';
   //------------------------------------------sensor------------
   
     if(substr($input015,-5,1)== '1')  {
      $loop1_ent5 = "blue";
      $loop1str_ent5 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
     } else {
      $loop1_ent5 = "red";
      $loop1str_ent5 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
     }
       if(substr($input015,-6,1)== '1')  {
      $loop2_ent5 = "blue";
      $loop2str_ent5 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
     } else {
      $loop2_ent5 = "red";
      $loop2str_ent5 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
     }
       if(substr($input015,-11,1)== '1')  {
      $loop1_ext5 = "blue";
      $loop1str_ext5 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
     } else {
      $loop1_ext5 = "red";
      $loop1str_ext5 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		

     }  if(substr($input015,-12,1)== '1')  {
      $loop2_ext5 = "blue";
      $loop2str_ext5 = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
     } else {
      $loop2_ext5 = "red";
      $loop2str_ext5 = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
     }

     if(substr($output015,-4,1)== '1')  {
      $alb_ent5 = "blue";
      $albstr_ent5 = "OPEN";
     } else {
      $alb_ent5 = "red";
      $albstr_ent5 = "CLOSE";
     }

      if(substr($output015,-8,1)== '1')  {
      $alb_ext5 = "blue";
      $albstr_ext5 = "OPEN";
     } else {
      $alb_ext5 = "red";
      $albstr_ext5 = "CLOSE";
     }

	echo '<td><font color= '.$loop1_ent5.' size=3>'.$loop1str_ent5.'</font></td>';
	echo '<td><font color='.$loop2_ent5.' size=3>'.$loop2str_ent5.'</font></td>';	
	echo '<td><font color='.$loop1_ext5.' size=3>'.$loop1str_ext5.'</font></td>';
	echo '<td><font color='.$loop2_ext5.' size=3>'.$loop2str_ext5.'</font></td>';	
	echo '<td> <font color='.$alb_ent5 .' size=3>'.$albstr_ent5 .'</font></td>';
	echo '<td> <font color='.$alb_ext5 .' size=3>'.$albstr_ext5 .'</font></td>';
		
	echo '<td><font color=green size=2>'.$epc_dat_ent015.'</font> <br><font color=green size=2>'.$date_park_ent015.'</font></td>';	
	echo '<td><font color=blue size=2>'.$epc_dat_ext015.'</font> <br><font color=blue size=2>'.$date_park_ext015.'</font></td>';
	echo '<td><font color=green size=2>'.$mfgno1015.'</font> <br><font color=green size=2>'.$date_mfgno1015.'</font></td>';
	echo '<td><font color=blue size=2>'.$mfgno2015.'</font> <br><font color=blue size=2>'.$date_mfgno2015.'</font></td>';
		
	//status alert HEARTBEAT
	echo '<td>';
	if($op_xport015 == '0') 
		{
			echo '<i class="fas fa-heart" style="font-size:25px;color:red;"></i>';
		} 
	elseif ($op_xport015 == '1')
		{
			echo '<i class="fas fa-heart" style="font-size:35px;color:red;"></i>';	
		}
	elseif ($op_xport015 == '2')
		{
			echo '<i class="fas fa-heart" style="font-size:30px;color:red;"></i>';	
		}	
		
	echo '</td></tr>'; 
	
	//********************** END DISPLAY LANE 5 **********************
	
	
	
	
	echo'</table>';  //end table   
	
	
	//************************************************************************************
	//**********************   START DISPLAY AREA ****************************************
	//************************************************************************************
?>
</body>
 </html>