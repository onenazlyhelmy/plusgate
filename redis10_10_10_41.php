
<html lang="en">
	<head>
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/pagination.css" rel="stylesheet">

		<title>Teras Log</title>
		<link rel="icon" type="image/png" href="img/teras.png"/>
        <!--10/01/2019 arie add font-awesome-->
		<link href="assets/fontawesome/css/all.css" rel="stylesheet" />
		<link href="assets/fontawesome/css/fontawesome.css" rel="stylesheet">
		<link href="assets/fontawesome/css/brands.css" rel="stylesheet">
		<link href="assets/fontawesome/css/solid.css" rel="stylesheet">
	</head>
 </html>

<?php
  // 08-01-2019: arie add :refer file from redisstatus folder 
  $myfile = fopen("redisstatus/input01.txt", "r") or die("Unable to open file!");
  $input01= fgets($myfile);
  fclose($myfile);
  $myfile = fopen("redisstatus/output01.txt", "r") or die("Unable to open file!");
  $output01= fgets($myfile);
  fclose($myfile);
  $myfile = fopen("redisstatus/mfgno201.txt", "r") or die("Unable to open file!");
  $mfgno201= fgets($myfile);
  fclose($myfile);
  $myfile = fopen("redisstatus/mfgno101.txt", "r") or die("Unable to open file!");
  $mfgno101= fgets($myfile);
  fclose($myfile);
  $myfile = fopen("redisstatus/date_mfgno101.txt", "r") or die("Unable to open file!");
  $date_mfgno101= fgets($myfile);

  fclose($myfile);
  $myfile = fopen("redisstatus/date_mfgno201.txt", "r") or die("Unable to open file!");
  $date_mfgno201= fgets($myfile);
    fclose($myfile);
    $myfile = fopen("redisstatus/op_xport01.txt", "r") or die("Unable to open file!");
    $op_xport01= fgets($myfile);
  //  echo   $op_xport01;
      fclose($myfile);

      $myfile = fopen("redisstatus/epc_dat_ent01.txt", "r") or die("Unable to open file!");
      $epc_dat_ent01= fgets($myfile);
        fclose($myfile);

        $myfile = fopen("redisstatus/date_park_ent01.txt", "r") or die("Unable to open file!");
        $date_park_ent01= fgets($myfile);
          fclose($myfile);

          $myfile = fopen("redisstatus/epc_dat_ext01.txt", "r") or die("Unable to open file!");
          $epc_dat_ext01= fgets($myfile);
            fclose($myfile);

            $myfile = fopen("redisstatus/date_park_ext01.txt", "r") or die("Unable to open file!");
            $date_park_ext01= fgets($myfile);
              fclose($myfile);

              $myfile = fopen("redisstatus/app.rfid_ent01.txt", "r") or die("Unable to open file!");
              $rfid_ent01= fgets($myfile);
                fclose($myfile);
                $myfile = fopen("redisstatus/app.rfid_ext01.txt", "r") or die("Unable to open file!");
                $rfid_ext01= fgets($myfile);
                  fclose($myfile);


    include 'ss/ss.php';

    echo '<table style="width:100%;text-align:center;background-color:#d9d9d9;" border=0 class="table table-striped table-bordered"> ';

   echo '</td></tr><tr><td> ';

     echo '</td></tr><tr><td> ';
	
	// 08-01-2019: arie add : Display record here
	//RFID ENT STATUS 
    if($rfid_ent01 =='1' )
		{       
			echo '<i class="far fa-lightbulb" style="font-size:30px;color:green;"></i>';		
		} 
		else 
		{
			echo '<i class="far fa-lightbulb" style="font-size:30px;color:red;"></i>';  
		}
  
	//RFID EXIT STATUS	
	echo '</td></tr><tr><td> ';
	if($rfid_ext01=='1' )
	{       
	echo '<i class="far fa-lightbulb" style="font-size:30px;color:green;"></i>';
	} 
	else 
	{
	echo '<i class="far fa-lightbulb" style="font-size:30px;color:red;"></i>';
	}
     echo '</td></tr> ';   
   //------------------------------------------sensor------------
   
     //LOOP1 ENTRY
	 if(substr($input01,-5,1)== '1')  {
      $loop1_ent = "blue";      
	  $loop1str_ent = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';
     } else {
      $loop1_ent = "red";      
	  $loop1str_ent = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';
     }
    
	//LOOP2 ENTRY 
	if(substr($input01,-6,1)== '1')  
	{
		$loop2_ent = "blue";      
		$loop2str_ent = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';
	} else 
	{
		$loop2_ent = "red";      
		$loop2str_ent = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';
	}
	
	//LOOP1 EXIT 	
	if(substr($input01,-11,1)== '1')  
	{
		$loop1_ext = "blue";
		$loop1str_ext = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
	} else 
	{
		$loop1_ext = "red";
		$loop1str_ext = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
    }  
	 
	//LOOP2 EXIT 
	if(substr($input01,-12,1)== '1')  
	{
		$loop2_ext = "blue";
		$loop2str_ext = '<i class="fas fa-car-side" style="font-size:30px;color:green;"></i>';		
	} else 
	{
		$loop2_ext = "red";
		$loop2str_ext = '<i class="fas fa-car-side" style="font-size:30px;color:red;"></i>';		
	}

	//ENTRY ALB
	if(substr($output01,-4,1)== '1')  
	{
		$alb_ent = "blue";
		$albstr_ent = "OPEN";
	} else 
	{
		$alb_ent = "red";
		$albstr_ent = "CLOSE";
	}

	//EXIT ALB
	if(substr($output01,-8,1)== '1')  
	{
		$alb_ext = "blue";
		$albstr_ext = "OPEN";
	} else 
	{
		$alb_ext = "red";
		$albstr_ext = "CLOSE";
	}

	echo '</td></tr><tr><td> <font color= '.$loop1_ent.' size=3>'.$loop1str_ent.'</font>';
	echo '</td></tr><tr><td><font color='.$loop2_ent.' size=3>'.$loop2str_ent.'</font>'; 
	echo '</td></tr><tr><td><font color='.$loop1_ext.' size=3>'.$loop1str_ext.'</font>';
	echo '</td></tr><tr><td><font color='.$loop2_ext.' size=3>'.$loop2str_ext.'</font>'; 
	echo '</td></tr><tr><td> <font color='.$alb_ent .' size=3>'.$albstr_ent .'</font>';
	echo '</td></tr><tr><td> <font color='.$alb_ext .' size=3>'.$albstr_ext .'</font>';
	//------------------------------------------------------------------------
	
	//Value MFG no & datetime
	echo '</td></tr><tr><td><font color=grey size=2></font><br><font color=green size=2>'.$epc_dat_ent01.'</font> <br><font color=green size=2>'.$date_park_ent01.'</font>';
	echo '</td></tr><tr><td><font color=blue size=2>'.$epc_dat_ext01.'</font> <br><font color=blue size=2>'.$date_park_ext01.'</font>';
	echo '</td></tr><tr><td>  <br>  <font color=green size=2>'.$mfgno101.'</font> <br><font color=green size=2>'.$date_mfgno101.'</font>';     
	echo '</td></tr><tr><td><font color=blue size=2>'.$mfgno201.'</font> <br><font color=blue size=2>'.$date_mfgno201.'</font>';
	echo '</td></tr></table>';    
		
	//status alert
	if($op_xport01 == '0') 
		{
			echo '<tr><td><i class="fas fa-heart" style="font-size:25px;color:red;"></i></td></tr>';
		} 
	elseif ($op_xport01 == '1')
		{
			echo '<tr><td><i class="fas fa-heart" style="font-size:35px;color:red;"></i></td></tr>';	
		}
	elseif ($op_xport01 == '2')
		{
			echo '<tr><td><i class="fas fa-heart" style="font-size:30px;color:red;"></i></td></tr>';	
		}	
?>
