<?php
class Database extends PDO
{
    public static $dbName = 'stardb' ;
    public static $dbHost = 'localhost' ;
        // public static $dbHost = '10.224.56.63' ;
    public static $dbUsername = 'root';
    public static $dbUserPassword = 'r00t123!';

    public static $cont  = null;

    public function __construct() {
        die('Init function is not allowed');
    }

    public static function connect()
    {
       // One connection through whole application
       if ( null == self::$cont )
       {
        try
        {
          self::$cont =  new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword);
        }
        catch(PDOException $e)
        {
          die($e->getMessage());
        }
       }
       return self::$cont;
    }

    public static function disconnect()
    {
        self::$cont = null;
    }

  public static function getdb()
    {
      return  self::$dbHost;
    }

     public static function getusrnm()
    {
      return  self::$dbUsername;
    }

     public static function getpass()
    {
      return  self::$dbUserPassword;
    }

}
?>
