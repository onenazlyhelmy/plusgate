<?php
     include '../function/function_db.php';
include '../ss/ss.php';
    $function_db = new function_db();
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];

      // echo "GET ID READ:".$_GET['id'];
    }

    if ( null==$id ) {
        header("Location: create.php");
    } else {
        $data = $function_db->getuserlist($id);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">


    <title>Info User</title>
    <link rel="icon" type="image/png" href="../img/teras.png"/>
      <!-- <img src='img/parrotsec.png' height='30px' width='30px'>
  <a href='https://www.parrotsec.org/' style="color:blue;font-size: 8pt; font-family:Lucida Console,Monaco, monospace;" > Powered by parrot security</a>-->

   <link href="../css/pagination.css" rel="stylesheet">
  <link   href="../css/bootstrap.min.css" rel="stylesheet">
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery-1.10.2.min.js"></script>

 <script type="text/javascript" src="../js/smoothie.js"></script>


  <link rel="icon" type="image/png" href="../img/teras.png"/>
    <!-- Bootstrap Styles-->
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="../assets/css/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="../assets/css/custom.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=fuck_u_mark' rel='stylesheet' type='text/css' />
    <link href="css/fixtab.css" rel="stylesheet">


    <!-- Metis Menu Js -->
    <script src="../assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="../assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="../assets/js/morris/morris.js"></script>

     <!-- Custom Js -->
    <script src="../assets/js/left-pane-slide.js"></script>

</head>

    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../index.php"><i class="fa fa-home"></i> <strong>USERS</strong></a>
            </div>

            <ul class="nav navbar-top-links navbar-right">

        </nav>

        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">

            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a href="../index.php"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>

                     <li>
                        <a href="../detectionCnt.php" style="display:none;" ><i class="fa fa-home"></i> Detection Graph</a>
                    </li>

                    <li>
                        <a href="../log.php"><i class="fa fa-edit"></i> Transactions Log </a>
                    </li>
                    <li>
                        <a href="create.php"><i class="fa fa-users"></i> Registration Users</a>
                    </li>
                      <li align='left'>
                        <a href="user.php" <?php echo $style; ?> ><i class="fa fa-users"></i> User Mgmnt</a>
                    </li>
                       <li align='left' >
                        <a href="../realtime.php" style="display:none;" ><i class="fa fa-users"  ></i> Realtime</a>
                    </li>
                    <li align='left'>
                     <a href="../hardware/"><i class="fa fa-users"></i> Hardware</a>
                   </li>
                          <li align='left'>
                        <a href="../logout666.php"><i class="fa fa-users"></i> Logout</a>
                    </li>
                  <!--   <li>
                        <a href="hdwareinfo/realtimehdinfo.php"><i class="fa fa-home"></i>Hardware Info</a>
                    </li>
          <li>
                        <a href="#"><i class="fa fa-user"></i> Administration<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="create.php">User Maintenance</a>
                            </li>
                            <li>
                                <a href="wbrmt.html">WEB Remote</a>
                            </li>
                           </ul>

                      </li> -->

                        </ul>
                    </li>
                </ul>

            </div>
            </nav>


           <div id="page-wrapper">
            <div id="page-inner">
                <!-- /. ROW  -->

<body>
    <div class="container">

                <div class="span10 offset1">
                    <div class="row">
                        <h3>Information User</h3>
                    </div>


                    <div class="form-horizontal" >
                       <div class="control-group">
                        <label class="control-label">ID:</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['idusertag'];?>
                            </label>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Name:</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['users_names'];?>
                            </label>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">EPC-TAGID:</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['users_tagid'];?>
                            </label>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">MFG No:</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['employee_id'];?>
                            </label>
                        </div>
                      </div>
                          <div class="control-group">
                        <label class="control-label">Plate No:</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['users_plno'];?>
                            </label>
                        </div>
                      </div>

                         <div class="control-group">
                        <label class="control-label">User Status:</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['users_status'];?>
                            </label>
                        </div>
                      </div>

                   <div class="control-group">
                        <label class="control-label">Org</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['users_org'];?>
                            </label>
                        </div>
                      </div>


                   <div class="control-group">
                        <label class="control-label">Remark</label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['users_remark'];?>
                            </label>
                        </div>
                      </div>

                        <div class="form-actions">

                          <a class="btn btn-success" href="create.php">Back</a>

                        <!--   <a class="btn btn-success" href="update.php?id=<?php echo $_GET['id']; ?> ">Update</a> -->
                       </div>


                    </div>
                </div>

    </div> <!-- /container -->
  </body>
</html>
