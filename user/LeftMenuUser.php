
<?php
 error_reporting(0);

?>

<!DOCTYPE html>
<html lang="en">
<title>Home</title>

<head>  
  <!--10/01/2019 arie add font-awesome-->
  <link href="../assets/fontawesome/css/all.css" rel="stylesheet" />
  <link href="../assets/fontawesome/css/fontawesome.css" rel="stylesheet">
  <link href="../assets/fontawesome/css/brands.css" rel="stylesheet">
  <link href="../assets/fontawesome/css/solid.css" rel="stylesheet">
</head>

<body onLoad="createTimeline();detectbrow()">
	
	<script src="js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="js/smoothie.js"></script>
	<link rel="icon" type="image/png" href="img/teras.png"/>
	
	<!-- Bootstrap Styles-->
	<link href="css/pagination.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="js/smoothie.js"></script>
	<link rel="icon" type="image/png" href="img/teras.png"/>
	<!-- Bootstrap Styles-->
	<link href="assets/css/bootstrap.css" rel="stylesheet" />
	
	<!-- FontAwesome Styles-->
	<link href="assets/css/font-awesome.css" rel="stylesheet" />	
	
	<!-- Morris Chart Styles-->
	<link href="assets/css/morris-0.4.3.min.css" rel="stylesheet" />
	<!-- Custom Styles-->
	<link href="assets/css/custom.css" rel="stylesheet" />
	<!-- Google Fonts-->
	<link href="css/fixtab.css" rel="stylesheet">

	<!-- Metis Menu Js -->
	<script src="assets/js/jquery.metisMenu.js"></script>
	<!-- Morris Chart Js -->
	<script src="assets/js/morris/raphael-2.1.0.min.js"></script>
	<script src="assets/js/morris/morris.js"></script>

	<!-- Custom Js -->
	<script src="assets/js/left-pane-slide.js"></script>     
	<div id="wrapper">
		<nav class="navbar navbar-default top-navbar" role="navigation">
			<div class="navbar-header">				
				<a href="http://www.plus.com.my" target= blank><img height='90' width='100' src='../img/TerasLogo.png'></a>
				
			</div>
			
			<ul class="nav navbar-top-links navbar-right"><img height='150' width='600' src='../img/RTDMWord.png'></ul>
			
		</nav>

		<!--/. NAV TOP  -->
		<nav class="navbar-default navbar-side" role="navigation" align='left' >

		<div class="sidebar-collapse" >
		<ul class="nav" id="main-menu">
			<li>				
				<a href="../index.php"><i class="far fa-address-card" style="font-size:30px;color:white;"></i>&nbsp&nbsp Dashboard</a>
			</li>			
			<li>
				<a href="../log.php"><i class="fas fa-file-signature" style="font-size:30px;color:white;"></i>&nbsp&nbsp  Transactions Log </a>
			</li>
			<li>
				<a href="create.php"><i class="fas fa-users" style="font-size:30px;color:white;"></i>&nbsp&nbsp  Add Users</a>
			</li>
			<li align='left'>
				<a href="user.php" <?php echo $style; ?> ><i class="fas fa-user-edit" style="font-size:30px;color:white;"></i>&nbsp&nbsp  User Mgmnt</a>
			</li>
			
			<li align='left' >
				<a href="../hdwareinfo/realtimehdinfo.php" style="display:none;"><i class="fas fa-th"></i> Hardware Info</a>
			</li>
			<li align='left'>
				<a href="../hardware/"><i class="fas fa-th" style="font-size:30px;color:white;"></i>&nbsp&nbsp Hardware</a>
			</li>
			<li align='left'>
				<a href="../logout666.php"><i class="fas fa-door-open" style="font-size:30px;color:white;"></i>&nbsp&nbsp Logout</a>
			</li>
		</ul>		
		</div>
	</nav>
</body>
</html>
